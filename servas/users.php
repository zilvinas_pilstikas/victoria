<?php
	include_once './db_functions.php';
	$fn = new DB_Functions();
	$method = $_SERVER['REQUEST_METHOD'];
	header('Content-type: aplication/json');
	if ($method=='POST') {
		$user_name = $_POST['user_name'];
		$user_username = $_POST['user_username'];
		$user_password = $_POST['user_password'];
		$result = $fn->registerUser($user_name, $user_username, $user_password);
		echo json_encode(array("response"=>$result));
	} else if ($method=='GET') {
		$user_username = $_GET['user_username'];
		$user_password = $_GET['user_password'];
		$result = $fn->checkUser($user_username, $user_password);
		echo json_encode(array("response"=>$result));
	} else {
		http_response_code(404);
	}
?>