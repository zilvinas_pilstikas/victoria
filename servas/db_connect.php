<?php
class DB_Connect {
	
	function __construct(){
		
	}
	public function connect() {
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_DATABASE);
		return $con; 
	}
	public function close() {
		mysqli_close();
	}
}
?>