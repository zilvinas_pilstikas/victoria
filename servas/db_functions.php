<?php
class DB_Functions {
	
	private $db;
	private $con;
	
	function __construct(){
		include_once './db_connect.php';
		$this->db = new DB_Connect();
	}
	
	
	//--------------------USER FUNCTIONS---------------------
	public function registerUser($user_name,$user_username,$user_password) {
		$this->con = $this->db->connect();
		if (!isset($user_name) && !isset($user_username) && !isset($user_password)){
			http_response_code(400);
			return FAILED;
		}
		$check_username = "select * from " . TABLE_USERS . " where user_username='$user_username'";
		$run_check = mysqli_query($this->con, $check_username);
		$row_check = mysqli_num_rows($run_check);
		if ($row_check > 0 || strlen($user_username) < 1) {
			http_response_code(400);
			return BAD_USERNAME;
		} else {
			$query = "insert into " . TABLE_USERS . " (user_name,user_username,user_password) values('$user_name','$user_username','$user_password')";
			$result = mysqli_query($this->con, $query);
			
			$queryOauth = "insert into " . TABLE_OAUTH_CLIENTS . " (client_id,client_secret, redirect_uri) values('$user_username','$user_password', 'http://fake/')";
			$resultOauth = mysqli_query($this->con, $queryOauth);
		
			$this->db->close();
			if ($result && $resultOauth) {
				http_response_code(201);
				return SUCCESS;
			} else {
				http_response_code(400);
				return FAILED;
			}
		}		
	}
	
	public function checkUser($user_username, $user_password){
		$this->con = $this->db->connect();
		$check_user = "select * from " . TABLE_USERS . " where user_username='$user_username' AND user_password='$user_password'";
		$run_check = mysqli_query($this->con, $check_user);
		$row_check = mysqli_num_rows($run_check);
		$this->db->close();
		
		if ($row_check == 1){
			$user = mysqli_fetch_array($run_check);
			$user_name = $user['user_name'];
			http_response_code(200);
			return SUCCESS . ":" . $user_name;
		} else {
			http_response_code(404);
			return FAILED;
		}
	}
	
	public function getAllUsers() {
		$this->con = $this->db->connect();
		$query = "select * FROM " . TABLE_USERS;
		$result = mysqli_query($this->con, $query);
		$this->db->close();
		return $result;
	}
	
	//----------------------CATEGORY FUNCTIONS----------------------------------
	public function addCategory($c_title, $c_username, $c_public_status, $c_image, $c_description, $c_sync_status, $c_password, $c_password_status) {
		if (!isset($c_title) && !isset($c_username) && !isset($c_image)){
			http_response_code(400);
			return FAILED . ":" . "-1";
		}
		$name = $c_title . $c_username;
		$this->con = $this->db->connect();
		$query = "insert into " . TABLE_CATEGORIES . " (category_title, category_username, category_public_status, category_image, category_description, category_sync_status, category_password, category_password_status) values('$c_title','$c_username','$c_public_status','$name','$c_description','$c_sync_status','$c_password','$c_password_status')";
		
		
		$result = mysqli_query($this->con, $query);
		$cat_id = $this->con->insert_id;
		file_put_contents(CATEGORIES_IMAGES . "/$name.jpg",base64_decode($c_image));
		
		$this->db->close();
		if ($result) {
			http_response_code(201);
			return SUCCESS . ":" . $cat_id;
		} else {
			http_response_code(400);
			return FAILED . ":" . "-1";
		} 
	}
	
	public function updateCategory($c_id, $c_title, $c_username, $c_public_status, $c_image, $c_description, $c_sync_status, $c_password, $c_password_status) {
		$this->con = $this->db->connect();
		
		$token = $_GET['access_token'];
		$select_client = "select client_id FROM oauth_access_tokens where access_token='$token'";
		$run_client=mysqli_query($this->con, $select_client);
		$row_client=mysqli_fetch_array($run_client);
		$client=$row_client['client_id'];
		
		if (!isset($c_id) && !isset($c_title) && !isset($c_username) && !isset($c_image)){
			http_response_code(400);
			return FAILED;
		}
		if ($client != $c_username){
			http_response_code(401);
			return FAILED;
		}
		$name = $c_title . $c_username;
		$query = "update " . TABLE_CATEGORIES . " set category_title='$c_title', category_username='$c_username', category_public_status='$c_public_status', category_image='$name', category_description='$c_description', category_sync_status='$c_sync_status', category_password='$c_password', category_password_status='$c_password_status' where category_id='$c_id'";
		
		$result = mysqli_query($this->con, $query);
		
		unlink(CATEGORIES_IMAGES . "/$name.jpg");
		file_put_contents(CATEGORIES_IMAGES . "/$name.jpg",base64_decode($c_image));
		
		$this->db->close();
		if (mysqli_affected_rows($this->con) > 0) {
			http_response_code(200);
			return SUCCESS;
		} else {
			http_response_code(400);
			return FAILED;
		} 
	}
	
	public function getCategoriesID($c_username) {
		$this->con = $this->db->connect();
		$query = "select category_id FROM " . TABLE_CATEGORIES . " where category_username='$c_username'";
		$cat_ids = mysqli_query($this->con, $query);
		$i = 0;
		while($row_cat_id=mysqli_fetch_array($cat_ids)){
			$cat_ids_array[$i++] = $row_cat_id['category_id'];
		}
		$this->db->close();
		if (isset($cat_ids_array)){
			http_response_code(200);
		} else {
			http_response_code(400);
		}
		return $cat_ids_array;
	}
	
	public function getCategory($c_id) {
		$this->con = $this->db->connect();
		$query = "select * FROM " . TABLE_CATEGORIES . " where category_id='$c_id'";
		$category = mysqli_query($this->con, $query);
		$i = 0;
		while($row_cat=mysqli_fetch_array($category)){
			$img = $row_cat['category_image'];
			
			$result = array(
				'category_id' => $row_cat['category_id'],
				'category_title' => $row_cat['category_title'],
				'category_username' => $row_cat['category_username'],
				'category_public_status' => $row_cat['category_public_status'],
				'category_description' => $row_cat['category_description'],
				'category_password' => $row_cat['category_password'],
				'category_password_status' => $row_cat['category_password_status'],
				'category_image' => base64_encode(file_get_contents(CATEGORIES_IMAGES . "/$img.jpg"))
			);
		}
		$this->db->close();
		if ($result==null) {
			http_response_code(404);
			return FAILED;
		}
		http_response_code(200);
		return $result;
	}
	
	public function deleteCategory($c_id) {
		$this->con = $this->db->connect();
		$token = $_GET['access_token'];
		
		$select_client = "select client_id FROM oauth_access_tokens where access_token='$token'";
		$run_client=mysqli_query($this->con, $select_client);
		$row_client=mysqli_fetch_array($run_client);
		$client=$row_client['client_id'];
		
		
		$select_name = "select category_title, category_username FROM " . TABLE_CATEGORIES . " where category_id='$c_id'";
		$run_select = mysqli_query($this->con, $select_name);
		$row = mysqli_fetch_array($run_select);
		$c_username = $row['category_username'];
		$c_title = $row['category_title'];
		
		if ($client != $c_username){
			http_response_code(401);
			return FAILED;
		}
		
		$name = $c_title . $c_username;
		$query = "delete FROM " . TABLE_CATEGORIES . " where category_id='$c_id'";
		$exc = mysqli_query($this->con, $query);
		if (mysqli_affected_rows($this->con) > 0) {
			unlink(CATEGORIES_IMAGES . "/$name.jpg");
			http_response_code(200);
			return SUCCESS;
		} else {
			http_response_code(400);
			return FAILED;
		}
	}
	
	//----------------------------QUESTIONS FUNCTIONS----------------------------
	public function addQuestion($fk_category_id, $q_creator, $q_owner, $q_type, $q_question, $q_answers, $q_correct_answers, $q_sync_status) {
		$this->con = $this->db->connect();
		if ($fk_category_id==null || $q_question==null || $q_answers==null || $q_correct_answers==null) {
			http_response_code(400);
			return FAILED . ":" . "-1";
		}
		$query = "insert into " . TABLE_QUESTIONS . " (fk_category_id, question_creator_username, question_owner_username, question_type, question_question, question_answers, question_correct_answers, question_sync_status) values('$fk_category_id','$q_creator','$q_owner','$q_type','$q_question','$q_answers','$q_correct_answers','$q_sync_status')";
		
		$result = mysqli_query($this->con, $query);
		$quest_id = $this->con->insert_id;
		
		$this->db->close();
		if ($result) {
			http_response_code(201);
			return SUCCESS . ":" . $quest_id;
		} else {
			http_response_code(400);
			return FAILED . ":" . "-1"; 
		}  
	}
	public function getQuestion($q_id) {
		$this->con = $this->db->connect();
		$query = "select * FROM " . TABLE_QUESTIONS . " where question_id='$q_id'";
		$question = mysqli_query($this->con, $query);
		$i = 0;
		while($row_cat=mysqli_fetch_array($question)){
			
			$result = array(
				'question_id' => $row_cat['question_id'],
				'fk_category_id' => $row_cat['fk_category_id'],
				'question_type' => $row_cat['question_type'],
				'question_question' => $row_cat['question_question'],
				'question_creator_username' => $row_cat['question_creator_username'],
				'question_owner_username' => $row_cat['question_owner_username'],
				'question_answers' => $row_cat['question_answers'],
				'question_correct_answers' => $row_cat['question_correct_answers']
			);
		}
		$this->db->close();
		if (isset($result)){
			http_response_code(200);
		} else {
			http_response_code(400);
		}
		return $result;
	}
	
	public function getQuestionsID($q_owner_username) {
		$this->con = $this->db->connect();
		$query = "select question_id FROM " . TABLE_QUESTIONS . " where question_owner_username='$q_owner_username'";
		$quest_ids = mysqli_query($this->con, $query);
		$i = 0;
		while($row_quest_id=mysqli_fetch_array($quest_ids)){
			$quest_ids_array[$i++] = $row_quest_id['question_id'];
		}
		$this->db->close();
		if (isset($quest_ids_array)){
			http_response_code(200);
		} else {
			http_response_code(400);
		}
		return $quest_ids_array;
	}
} 
?>