<?php
	include_once './db_functions.php';
	$fn = new DB_Functions();
	header('Content-type: aplication/json');
	$method = $_SERVER['REQUEST_METHOD'];
	if ($method=='POST') {
		$fk_category_id= $_POST['fk_category_id'];
		$question_creator_username= $_POST['question_creator_username'];
		$question_owner_username= $_POST['question_owner_username'];
		$question_type= $_POST['question_type'];
		$question_question= $_POST['question_question'];
		$question_answers= $_POST['question_answers'];
		$question_correct_answers= $_POST['question_correct_answers'];
		$question_sync_status= $_POST['question_sync_status'];
	
		$result = $fn->addQuestion($fk_category_id, $question_creator_username, $question_owner_username, $question_type, $question_question, $question_answers, $question_correct_answers, $question_sync_status);
		$result_array = explode(":",$result);
		$response = $result_array[0];
		$quest_id = $result_array[1];
		echo json_encode(array("response"=>$response, "quest_id"=>$quest_id));
	} else if ($method=='GET' and isset($_GET['question_id'])) {
		$question_id = $_GET['question_id'];
		$result = $fn->getQuestion($question_id);
		echo json_encode(array($result));
	} else if ($method=='GET' and isset($_GET['question_owner_username'])){
		$question_owner_username = $_GET['question_owner_username'];
		$quest_ids_array = $fn->getQuestionsID($question_owner_username);
		$string_id = implode(",", $quest_ids_array);
		echo json_encode(array("response"=>$string_id));
	} else {
		http_response_code(404);
		echo json_encode(array("response"=>"BAD REQUEST METHOD"));
	}
?>