<?php
	include_once './db_functions.php';
	$fn = new DB_Functions();
	$method = $_SERVER['REQUEST_METHOD'];
	header('Content-type: aplication/json');
	if ($method=='POST') {
		$category_title = $_POST['category_title'];
		$category_username = $_POST['category_username'];
		$category_public_status = $_POST['category_public_status'];
		$category_image = $_POST['category_image'];
		$category_description = $_POST['category_description'];
		$category_sync_status = $_POST['category_sync_status'];
		$category_password = $_POST['category_password'];
		$category_password_status = $_POST['category_password_status'];
		
		$result = $fn->addCategory($category_title, $category_username, $category_public_status, $category_image, $category_description, $category_sync_status, $category_password, $category_password_status);
		$result_array = explode(":",$result);
		$response = $result_array[0];
		$cat_id = $result_array[1];
		echo json_encode(array("response"=>$response, "cat_id"=>$cat_id));
	} else if ($method=='GET' and isset($_GET['category_id'])) {
		$category_id = $_GET['category_id'];
		$result = $fn->getCategory($category_id);
		echo json_encode(array($result));
	} else if ($method=='GET' and isset($_GET['category_username'])) {
		$category_username = $_GET['category_username'];
		$cat_ids_array = $fn->getCategoriesID($category_username);
		$string_id = implode(",", $cat_ids_array);
		echo json_encode(array("response"=>$string_id));
	} else if ($method=='DELETE') {
		$category_id = $_GET['category_id'];
		$result = $fn->deleteCategory($category_id);
		echo json_encode(array("response"=>$result));
	} else if ($method=='PUT') {
		parse_str(file_get_contents("php://input"),$_PUT);
		$category_id = $_PUT['category_id'];
		$category_title = $_PUT['category_title'];
		$category_username = $_PUT['category_username'];
		$category_public_status = $_PUT['category_public_status'];
		$category_image = $_PUT['category_image'];
		$category_description = $_PUT['category_description'];
		$category_sync_status = $_PUT['category_sync_status'];
		$category_password = $_PUT['category_password'];
		$category_password_status = $_PUT['category_password_status'];
		
		$result = $fn->updateCategory($category_id, $category_title, $category_username, $category_public_status, $category_image, $category_description, $category_sync_status, $category_password, $category_password_status);
		echo json_encode(array("response"=>$result));
	} else {
		http_response_code(404);
	}
?>