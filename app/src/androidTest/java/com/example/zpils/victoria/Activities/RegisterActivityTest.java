package com.example.zpils.victoria.Activities;

import android.support.design.widget.TextInputLayout;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.example.zpils.victoria.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.sql.Time;
import java.util.Date;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Created by zpils on 2017-11-06.
 */
public class RegisterActivityTest {

    @Rule
    public ActivityTestRule<RegisterActivity> mActivityTestRule = new ActivityTestRule<RegisterActivity>(RegisterActivity.class);

    private RegisterActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void testRegister01() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText("Zilvinas"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        onView(allOf(withId(R.id.snackbar_text), withText("Username Already Exists")))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testRegister02() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText("Zilvinas"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu123"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        TextInputLayout textInputLayoutConfirmPassword = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutConfirmPassword);

        assertEquals("Password Does Not Matches", textInputLayoutConfirmPassword.getError().toString());
    }

    @Test
    public void testRegister03() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText("Client"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("NewClient" + new Date().getTime()), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        onView(allOf(withId(R.id.snackbar_text), withText("Registration Successful")))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testRegister04() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu123"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        TextInputLayout textInputLayoutName = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutName);

        assertEquals("Enter Full Name", textInputLayoutName.getError().toString());
    }

    @Test
    public void testRegister05() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText("Zilvinas"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu123"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        TextInputLayout textInputLayoutUsername = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutUsername);

        assertEquals("Enter Username", textInputLayoutUsername.getError().toString());
    }

    @Test
    public void testRegister06() throws Exception {
        onView(withId(R.id.textInputEditTextName))
                .perform(typeText("Zilvinas"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextConfirmPassword))
                .perform(typeText("nesakysiu123"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister))
                .perform((click()));

        TextInputLayout textInputLayoutPassword = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutPassword);

        assertEquals("Enter Password", textInputLayoutPassword.getError().toString());
    }

}