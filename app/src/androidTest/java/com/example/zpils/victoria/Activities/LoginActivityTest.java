package com.example.zpils.victoria.Activities;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.example.zpils.victoria.R;

import org.hamcrest.core.AllOf;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 * Created by zpils on 2017-11-05.
 */
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);

    private LoginActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }
    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void testLogin01() throws Exception {

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonLogin))
                .perform((click()));

        View view = mActivity.findViewById(R.id.snackbar_text);
        assertNull(view);
    }

    @Test
    public void testLogin02() throws Exception {

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysi"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonLogin))
                .perform((click()));

        View view = mActivity.findViewById(R.id.snackbar_text);
        assertNotNull(view);
    }

    @Test
    public void testLogin03() throws Exception {

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText("nesakysiu"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonLogin))
                .perform((click()));

        TextInputLayout textInputLayoutUsername = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutUsername);

        assertEquals("Enter Username", textInputLayoutUsername.getError().toString());
    }

    @Test
    public void testLogin04() throws Exception {

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonLogin))
                .perform((click()));

        TextInputLayout textInputLayoutUsername = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutUsername);

        assertEquals("Enter Username", textInputLayoutUsername.getError().toString());
    }

    @Test
    public void testLogin05() throws Exception {

        onView(withId(R.id.textInputEditTextUsername))
                .perform(typeText("Diganer"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextPassword))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonLogin))
                .perform((click()));

        TextInputLayout textInputLayoutPassword = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutPassword);

        assertEquals("Enter Password", textInputLayoutPassword.getError().toString());
    }
}