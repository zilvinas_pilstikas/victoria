package com.example.zpils.victoria.Activities;

import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.GeneralLocation;
import android.support.test.espresso.action.Press;
import android.support.test.rule.ActivityTestRule;
import android.widget.RadioButton;

import com.example.zpils.victoria.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.*;

/**
 * Created by zpils on 2017-11-08.
 */
public class NewQuestionActivityTest {

    @Rule
    public ActivityTestRule<NewQuestionActivity> mActivityTestRule = new ActivityTestRule<NewQuestionActivity>(NewQuestionActivity.class);

    private NewQuestionActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void testNewQuestion01() throws Exception {

        onView(withId(R.id.appCompatButtonAddQuestion)).
                perform(click());

        RadioButton radioButtonType = (RadioButton) mActivity.findViewById(R.id.radioCheckboxQuestion);
        assertEquals("Select A Question Type", radioButtonType.getError().toString());
    }
}