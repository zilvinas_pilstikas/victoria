package com.example.zpils.victoria.Activities;

import android.content.res.Resources;
import android.support.design.widget.TextInputLayout;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.example.zpils.victoria.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertEquals;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by zpils on 2017-11-06.
 */
public class NewCategoryActivityTest {

    @Rule
    public ActivityTestRule<NewCategoryActivity> mActivityTestRule = new ActivityTestRule<NewCategoryActivity>(NewCategoryActivity.class);

    private NewCategoryActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

    @Test
    public void testNewCategory01() throws Exception {
        onView(withId(R.id.textInputEditTextTitle))
                .perform(typeText("KategorijaTest"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextDescription))
                .perform(typeText("AprasymasTest"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonAddCategory))
                .perform((click()));

        onView(allOf(withId(R.id.snackbar_text), withText("Upload Category Image")))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testNewCategory02() throws Exception {
        onView(withId(R.id.textInputEditTextTitle))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextDescription))
                .perform(typeText("AprasymasTest"), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonAddCategory))
                .perform((click()));

        TextInputLayout textInputLayoutTitle = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutTitle);

        assertEquals("Enter Category Title", textInputLayoutTitle.getError().toString());
    }

    @Test
    public void testNewCategory03() throws Exception {
        onView(withId(R.id.textInputEditTextTitle))
                .perform(typeText("KategorijaTest"), closeSoftKeyboard());

        onView(withId(R.id.textInputEditTextDescription))
                .perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonAddCategory))
                .perform((click()));

        TextInputLayout textInputLayoutDescription = (TextInputLayout) mActivity.findViewById(R.id.textInputLayoutDescription);

        assertEquals("Enter Category Description", textInputLayoutDescription.getError().toString());
    }
}