package com.example.zpils.victoria.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.Model.Category;
import com.example.zpils.victoria.Model.Question;
import com.example.zpils.victoria.Model.User;
import com.example.zpils.victoria.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.zpils.victoria.R.id.listView;
import static com.example.zpils.victoria.R.id.nestedScrollView;

/**
 * Created by zpils on 2017-06-16.
 */

/**
 * Still need:
 *  finish main menu
 *  finish context menu
 *  start the game
 *
 *  fix activity return after multiple quest_inserts(i will redirect after quest insert to edit_quesst or list_uest activity and it should be solved)
 *
 *  Next i think i should make the actual game, that way i will be able to test local database.
 *  But also need to make question synchronization, well maybe after the game.
 */

public class UsersActivity extends AppCompatActivity implements ListView.OnItemClickListener{
    ArrayList<String> CATEGORIES_IMAGES;
    ArrayList<String> CATEGORIES_TITLES;
    ArrayList<Integer> CATEGORIES_ID;

    private SessionManager sessionManager;
    private final AppCompatActivity activity = UsersActivity.this;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private HashMap<String, String> userDetails;
    private String user_username;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        initObjects();

        sessionManager.checkLogin();
        if (!sessionManager.isLoggedIn()){
            finish();
        } else {
            setContentView(R.layout.activity_users);

            checkCategorySynchronization();
            getCategories();
            checkQuestionSynchronization();

            CategoryAdapter categoryAdapter = new CategoryAdapter();
            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(categoryAdapter);
            registerForContextMenu(listView);
            listView.setOnItemClickListener(this);
        }
    }

    private void initObjects(){
        sessionManager = new SessionManager(activity);
        databaseHelper = new DatabaseHelper(activity);
        appServerHelper = new AppServerHelper(activity);
        userDetails = sessionManager.getUserDetails();
        user_username = userDetails.get(SessionManager.KEY_USERNAME);
    }

    private void checkCategorySynchronization(){
        if (NetworkMonitor.checkNetworkConnection(activity)) {
            checkLocalCategorySynchronization();
            checkServerCategorySynchronization();
        }
    }

    private void checkLocalCategorySynchronization() {
        Cursor cursor = databaseHelper.getAllUnsynchronizedCategories(user_username);
        if (cursor != null) {
            if (cursor.moveToFirst()){
                do {
                    Category category = new Category(true);

                    category.setTitle(cursor.getString(cursor.getColumnIndex(Category.COLUMN_TITLE)));
                    category.setUsername(cursor.getString(cursor.getColumnIndex(Category.COLUMN_USERNAME)));
                    category.setDescription(cursor.getString(cursor.getColumnIndex(Category.COLUMN_DESCRIPTION)));
                    category.setImage(cursor.getString(cursor.getColumnIndex(Category.COLUMN_IMAGE)));
                    category.setPassword_status(cursor.getInt(cursor.getColumnIndex(Category.COLUMN_PASSWORD_STATUS)));
                    category.setPassword(cursor.getString(cursor.getColumnIndex(Category.COLUMN_PASSWORD)));
                    category.setPublic_status(cursor.getInt(cursor.getColumnIndex(Category.COLUMN_PUBLIC_STATUS)));
                    category.setSync_status(Category.SYNC_STATUS_OK);
                    final int cat_old_id = cursor.getInt(cursor.getColumnIndex(Category.COLUMN_ID));

                    appServerHelper.addCategory(category, new AppServerHelper.VolleyCallBack() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.contains("success")){
                                int cat_new_id = Integer.parseInt(result.split(":")[1]);
                                databaseHelper.updateCategorySyncStatusAndID(Category.SYNC_STATUS_OK, cat_old_id, cat_new_id);
                            }
                        }
                    });
                } while (cursor.moveToNext());
            }
        }
    }

    private void checkServerCategorySynchronization() {
        appServerHelper.getAllCategoriesID(user_username, new AppServerHelper.VolleyCallBack() {
            @Override
            public void onSuccess(String result) {
                int [] localIDs = databaseHelper.getAllCategoriesID(user_username);
                String[] serverIDs = result.split(",");
                for (String sID : serverIDs){
                    boolean idFounded = false;
                    for (int lID : localIDs){
                        if (lID == Integer.parseInt(sID)){
                            idFounded = true;
                            break;
                        }
                    }
                    if (!idFounded){
                        //get all information about not founded category from server and add to local database
                        appServerHelper.getCategory(Integer.parseInt(sID), new AppServerHelper.VolleyCallBack() {
                            @Override
                            public void onSuccess(String result) {
                                Category category = new Category(true);
                                String[] res_array = result.split("<=>");

                                category.setId(Integer.parseInt(res_array[0]));
                                category.setTitle(res_array[1]);
                                category.setUsername(res_array[2]);
                                category.setPublic_status(Integer.parseInt(res_array[3]));
                                category.setDescription(res_array[4]);
                                category.setPassword(res_array[5]);
                                category.setPassword_status(Integer.parseInt(res_array[6]));
                                category.setImage(res_array[7]);
                                category.setSync_status(Category.SYNC_STATUS_OK);

                                databaseHelper.addCategory(category,category.getId());
                                Intent intentSelf = new Intent(getApplicationContext(), UsersActivity.class);
                                startActivity(intentSelf);
                                finish();
                            }
                        });
                    }
                }
            }
        });
    }

    private void checkQuestionSynchronization(){
        if (NetworkMonitor.checkNetworkConnection(activity)) {
            checkLocalQuestionSynchronization();
            checkServerQuestionSynchronization();
        }
    }

    private void checkLocalQuestionSynchronization() {
        Cursor cursor = databaseHelper.getAllUnsynchronizedQuestions(user_username);
        if (cursor != null) {
            if (cursor.moveToFirst()){
                do {
                    Question question = new Question();

                    question.setFk_category_id(cursor.getInt(cursor.getColumnIndex(Question.COLUMN_FK_CATEGORY_ID)));
                    question.setType(cursor.getInt(cursor.getColumnIndex(Question.COLUMN_TYPE)));
                    question.setQuestion(cursor.getString(cursor.getColumnIndex(Question.COLUMN_QUESTION)));
                    question.setCreator_username(cursor.getString(cursor.getColumnIndex(Question.COLUMN_CREATOR_USERNAME)));
                    question.setOwner_username(cursor.getString(cursor.getColumnIndex(Question.COLUMN_OWNER_USERNAME)));
                    question.setSync_status(Question.SYNC_STATUS_OK);

                    String correct_answers = cursor.getString(cursor.getColumnIndex(Question.COLUMN_CORRECT_ANSWERS));
                    String[] correct_ansArray = correct_answers.substring(1,correct_answers.length()-1).split(",");
                    question.setCorrect_answers(correct_ansArray);

                    String answers = cursor.getString(cursor.getColumnIndex(Question.COLUMN_ANSWERS));
                    String[] ansArray = answers.substring(1,answers.length()-1).split(",");
                    question.setAnswers(ansArray);

                    final int quest_old_id = cursor.getInt(cursor.getColumnIndex(Question.COLUMN_ID));

                    appServerHelper.addQuestion(question, new AppServerHelper.VolleyCallBack() {
                        @Override
                        public void onSuccess(String result) {
                            if (result.contains("success")){
                                int quest_new_id = Integer.parseInt(result.split(":")[1]);
                                databaseHelper.updateQuestionSyncStatusAndID(Question.SYNC_STATUS_OK, quest_old_id, quest_new_id);
                            }
                        }
                    });
                } while (cursor.moveToNext());
            }
        }
    }

    private void checkServerQuestionSynchronization() {
        appServerHelper.getAllQuestionsID(user_username, new AppServerHelper.VolleyCallBack() {
            @Override
            public void onSuccess(String result) {
                int [] localIDs = databaseHelper.getAllQuestionsID(user_username);
                String[] serverIDs = result.split(",");
                for (String sID : serverIDs){
                    boolean idFounded = false;
                    for (int lID : localIDs){
                        if (lID == Integer.parseInt(sID)){
                            idFounded = true;
                            break;
                        }
                    }
                    if (!idFounded){
                        //get all information about not founded question from server and add to local database
                        appServerHelper.getQuestion(Integer.parseInt(sID), new AppServerHelper.VolleyCallBack() {
                            @Override
                            public void onSuccess(String result) {
                                Question question = new Question();
                                String[] res_array = result.split("<=>");

                                question.setId(Integer.parseInt(res_array[0]));
                                question.setFk_category_id(Integer.parseInt(res_array[1]));
                                question.setType(Integer.parseInt(res_array[2]));
                                question.setQuestion(res_array[3]);
                                question.setCreator_username(res_array[4]);
                                question.setOwner_username(res_array[5]);

                                String answers = res_array[6];
                                String[] ansArray = answers.substring(1,answers.length()-1).split(",");
                                question.setAnswers(ansArray);

                                String correct_answers = res_array[7];
                                String[] correct_ansArray = correct_answers.substring(1,correct_answers.length()-1).split(",");
                                question.setCorrect_answers(correct_ansArray);
                                question.setSync_status(Question.SYNC_STATUS_OK);

                                databaseHelper.addQuestion(question,question.getId());
                                Intent intentSelf = new Intent(getApplicationContext(), UsersActivity.class);
                                startActivity(intentSelf);
                                finish();
                            }
                        });
                    }
                }
            }
        });
    }

    private void getCategories(){
        int i = 0;
        Cursor cursor = databaseHelper.getAllCategories(user_username);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                CATEGORIES_IMAGES = new ArrayList<String>(cursor.getCount());
                CATEGORIES_TITLES = new ArrayList<String>(cursor.getCount());
                CATEGORIES_ID = new ArrayList<Integer>(cursor.getCount());
                do {
                    CATEGORIES_TITLES.add(i,cursor.getString(cursor.getColumnIndex(Category.COLUMN_TITLE)));
                    CATEGORIES_IMAGES.add(i,cursor.getString(cursor.getColumnIndex(Category.COLUMN_IMAGE)));
                    CATEGORIES_ID.add(i,cursor.getInt(cursor.getColumnIndex(Category.COLUMN_ID)));
                    i++;
                } while (cursor.moveToNext());
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.add_question_id:
                Intent intentNewQuestion = new Intent(getApplicationContext(), NewQuestionActivity.class);

                intentNewQuestion.putExtra("category_title",CATEGORIES_TITLES.get(info.position));
                intentNewQuestion.putExtra("category_id", CATEGORIES_ID.get(info.position));
                startActivity(intentNewQuestion);
                return true;
            case R.id.delete_category_id:
                if (NetworkMonitor.checkNetworkConnection(activity)){
                    final int index = info.position;
                    appServerHelper.deleteCategory(CATEGORIES_ID.get(index), new AppServerHelper.VolleyCallBack() {
                        @Override
                        public void onSuccess(String result) {
                            databaseHelper.deleteCategory(CATEGORIES_ID.get(index));
                            CATEGORIES_ID.remove(index);
                            CATEGORIES_IMAGES.remove(index);
                            CATEGORIES_TITLES.remove(index);
                        }
                    });
                    Intent intentSelf = new Intent(getApplicationContext(), UsersActivity.class);
                    startActivity(intentSelf);
                    finish();
                } else {
                    //Snack Bar to show error message that Internet connection is required
                    Toast.makeText(getApplicationContext(), R.string.internet_connection_required, Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.edit_category_id:
                Intent intentEditCategory = new Intent(getApplicationContext(), EditCategoryActivity.class);
                intentEditCategory.putExtra("category_id", CATEGORIES_ID.get(info.position));
                startActivity(intentEditCategory);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout){
            sessionManager.logoutUser();
            finish();
            return true;
        } else if (id == R.id.new_category){
            Intent intentNewCategory = new Intent(getApplicationContext(), NewCategoryActivity.class);
            startActivity(intentNewCategory);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentGame = new Intent(getApplicationContext(), GameActivity.class);
        intentGame.putExtra("category_id",CATEGORIES_ID.get(position));
        startActivity(intentGame);
    }

    class CategoryAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            if (CATEGORIES_IMAGES == null)
                return 0;
            else
                return CATEGORIES_IMAGES.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.category_layout, null);
            ImageView imageView_category = (ImageView) view.findViewById(R.id.imageView_category);
            TextView textView_category = (TextView) view.findViewById(R.id.textView_category);
            imageView_category.setImageBitmap(CryptoHelper.StringToBitMap(CATEGORIES_IMAGES.get(i)));
            textView_category.setText(CATEGORIES_TITLES.get(i));
            return view;
        }
    }
}
