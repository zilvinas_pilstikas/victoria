package com.example.zpils.victoria.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Model.Question;
import com.example.zpils.victoria.Model.User;
import com.example.zpils.victoria.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by zpils on 2017-08-21.
 */

/**
 * Pay more attention to trim()
 * Check if there are enough questions to start the game
 * Show correct answers after check
 * Show progress bar
 * Show correct/incorrect message in the middle of screen
 */

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = GameActivity.this;
    private NestedScrollView nestedScrollView;

    private AppCompatTextView appCompatTextViewQuestion;

    private RadioGroup radioGroupAnswers;
    private ArrayList<RadioButton> radioButtonArrayList;
    private ArrayList<CheckBox> checkBoxArrayList;
    private TextInputEditText textInputEditTextAnswer;

    private AppCompatButton appCompatButtonCheck;
    private AppCompatButton appCompatButtonContinue;

    private DatabaseHelper databaseHelper;
    private Question question;
    private int category_id;
    private int number_of_questions;
    private int[] available_question_id;
    private int[] random_unique_index;
    private int current_quest;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
        if (available_question_id.length != 0)
            updateQuestion();
    }

    private void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        appCompatTextViewQuestion = (AppCompatTextView) findViewById(R.id.appCompatTextViewQuestion);
        radioGroupAnswers = new RadioGroup(activity);
        radioButtonArrayList = new ArrayList<RadioButton>();
        checkBoxArrayList = new ArrayList<CheckBox>();
        textInputEditTextAnswer = new TextInputEditText(activity);
        appCompatButtonCheck = (AppCompatButton) findViewById(R.id.appCompatButtonCheck);
        appCompatButtonContinue = (AppCompatButton) findViewById(R.id.appCompatButtonContinue);
    }

    private void initListeners(){
        appCompatButtonCheck.setOnClickListener(this);
        appCompatButtonContinue.setOnClickListener(this);
    }

    private void initObjects() {
        question = new Question();
        databaseHelper = new DatabaseHelper(activity);
        category_id = getIntent().getIntExtra("category_id",-1);
        current_quest = 0;
        available_question_id = databaseHelper.getQuestionIDs(category_id);
        number_of_questions = 10;
        if (number_of_questions > available_question_id.length){
            number_of_questions = available_question_id.length;
        }
        if (available_question_id.length == 0) {
            Intent intentUser = new Intent(getApplicationContext(), UsersActivity.class);
            startActivity(intentUser);
            finish();
        }
        random_unique_index = new int[number_of_questions];
        Arrays.fill(random_unique_index,-1);
        initRandom_unique_index();

    }

    private void initRandom_unique_index(){
        Random random = new Random();
        for (int i = 0; i < number_of_questions; i++){

            int rnd = random.nextInt(available_question_id.length);

            boolean is_unique = true;
            for (int a = 0; a < random_unique_index.length; a++){
                if (random_unique_index[a] == rnd){
                    is_unique = false;
                }
            }
            if(is_unique){
                random_unique_index[i] = rnd;
            } else {
                i--;
            }
        }
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.appCompatButtonCheck:
                appCompatButtonCheck.setVisibility(View.GONE);
                appCompatButtonContinue.setVisibility(View.VISIBLE);
                disableFields();
                checkAnswer();
                current_quest++;
                break;
            case R.id.appCompatButtonContinue:
                if (current_quest != number_of_questions) {
                    appCompatButtonContinue.setVisibility(View.GONE);
                    appCompatButtonCheck.setVisibility(View.VISIBLE);
                    emptyInputEditText();
                    updateQuestion();
                } else {
                    Snackbar.make(nestedScrollView, "Completed!", Snackbar.LENGTH_LONG).show();
                }
        }
    }



    private void checkAnswer(){
        int quest_type = question.getType();
        String[] correct_answers = question.getCorrect_answers();

        if (quest_type == Question.CLOSED_QUESTION){
            int btn_id = radioGroupAnswers.getCheckedRadioButtonId();
            checkRadioAnswer(btn_id, correct_answers[0]);
        } else if (quest_type == Question.CHECKBOX_QUESTION){
            checkCheckboxAnswer(correct_answers);
        } else if (quest_type == Question.OPEN_QUESTION){
            checkOpenAnswer(correct_answers);
        }
    }

    private void checkRadioAnswer(int btn_id, String correct_answer){
        for (RadioButton rd : radioButtonArrayList){
            if (rd.getId() == btn_id){
                if (correct_answer.trim().equals(rd.getText().toString().trim())){
                    Snackbar.make(nestedScrollView, "Correct!", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(nestedScrollView, "Wrong!", Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    private void checkCheckboxAnswer(String[] correct_answers){
        int correct_count = 0;
        boolean correct = true;
        for (int i = 0; i < checkBoxArrayList.size(); i++){
            if (checkBoxArrayList.get(i).isChecked()){
                boolean part_correct = false;
                for (int a = 0; a < correct_answers.length; a++){
                    if (checkBoxArrayList.get(i).getText().toString().trim().equals(correct_answers[a].trim())){
                        part_correct = true;
                        correct_count++;
                        break;
                    }
                }
                if (!part_correct){
                    correct = false;
                    break;
                }
            }

        }
        if (correct && correct_count == correct_answers.length){
            Snackbar.make(nestedScrollView, "Correct!", Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(nestedScrollView, "Wrong!", Snackbar.LENGTH_LONG).show();
        }
    }

    private void checkOpenAnswer(String[] correct_answers){
        for (int i = 0; i < correct_answers.length; i++){
            if (correct_answers[i].trim().equals(textInputEditTextAnswer.getText().toString().trim())){
                Snackbar.make(nestedScrollView, "Correct!", Snackbar.LENGTH_LONG).show();
                return;
            }
            Snackbar.make(nestedScrollView, "Wrong!", Snackbar.LENGTH_LONG).show();
        }
    }

    private void updateQuestion(){
        question = databaseHelper.getQuestion(category_id,available_question_id[random_unique_index[current_quest]]);
        appCompatTextViewQuestion.setText(question.getQuestion());

        int quest_type = question.getType();
        String[] answers = question.getAnswers();
        if (quest_type == Question.CLOSED_QUESTION){
            for (int i = 0; i < answers.length; i++){
                createRadiobox(answers[i]);
            }
        } else if (quest_type == Question.CHECKBOX_QUESTION){
            for (int i = 0; i < answers.length; i++){
                createCheckbox(answers[i]);
            }
        } else if (quest_type == Question.OPEN_QUESTION){
            createTextInput();
        }
    }

    private void createRadiobox(String answer){
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompatGame);
        RadioButton radioButton = new RadioButton(activity);
        radioButton.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        radioButton.setId(findUnusedId(R.id.linearLayoutCompatGame));
        radioButton.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));

        radioButton.setText(answer);
        radioGroupAnswers.addView(radioButton);
        radioButtonArrayList.add(radioButton);

        linearLayoutCompat.removeView(radioGroupAnswers);
        linearLayoutCompat.addView(radioGroupAnswers,linearLayoutCompat.getChildCount()-2);
    }

    private void createCheckbox(String answer){
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompatGame);
        CheckBox checkBox = new CheckBox(activity);

        checkBox.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        checkBox.setId(findUnusedId(R.id.linearLayoutCompatGame));
        checkBox.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));
        checkBox.setText(answer);

        checkBoxArrayList.add(checkBox);
        linearLayoutCompat.addView(checkBox,linearLayoutCompat.getChildCount()-2);
    }

    private void createTextInput(){
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompatGame);

        textInputEditTextAnswer.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        textInputEditTextAnswer.setHint(R.string.hint_enter_the_answer);
        textInputEditTextAnswer.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));
        textInputEditTextAnswer.setHintTextColor(ContextCompat.getColor(activity, R.color.colorText));
        textInputEditTextAnswer.setId(findUnusedId(R.id.linearLayoutCompatGame));
        linearLayoutCompat.addView(textInputEditTextAnswer,linearLayoutCompat.getChildCount()-2);
    }

    private int findUnusedId(int startID) {
        while( findViewById(++startID) != null );
        return startID;
    }

    private void disableFields(){
        for (int i = 0; i < radioButtonArrayList.size(); i++){
            radioButtonArrayList.get(i).setEnabled(false);
        }
        for (int i = 0; i < checkBoxArrayList.size(); i++){
            checkBoxArrayList.get(i).setEnabled(false);
        }
        textInputEditTextAnswer.setEnabled(false);
    }

    private void emptyInputEditText(){
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompatGame);
        linearLayoutCompat.removeView(radioGroupAnswers);
        for (int i = 0; i < checkBoxArrayList.size(); i++){
            linearLayoutCompat.removeView(checkBoxArrayList.get(i));
        }
        linearLayoutCompat.removeView(textInputEditTextAnswer);
        radioGroupAnswers.removeAllViews();
        radioButtonArrayList.clear();
        checkBoxArrayList.clear();
        textInputEditTextAnswer.setText(null);
        textInputEditTextAnswer.setEnabled(true);
    }
}
