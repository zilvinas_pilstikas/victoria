package com.example.zpils.victoria.Database;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.Helper.VolleySingleton;
import com.example.zpils.victoria.Model.Category;
import com.example.zpils.victoria.Model.Question;
import com.example.zpils.victoria.Model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zpils on 2017-07-01.
 */

public class AppServerHelper {
    private Context context;
    public static final String TOKEN_URL = "http://zilpil.stud.if.ktu.lt/Victoria/token/";
    public static final String USERS_URL = "http://zilpil.stud.if.ktu.lt/Victoria/users/";
    public static final String CATEGORIES_URL = "http://zilpil.stud.if.ktu.lt/Victoria/categories/";
    public static final String QUESTIONS_URL = "http://zilpil.stud.if.ktu.lt/Victoria/questions/";

    SessionManager sessionManager;

    public AppServerHelper(Context context){
        this.context = context;
        sessionManager = new SessionManager(this.context);


        //getting current time
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        try {
            sdf.parse(sessionManager.getExpireDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar expire = sdf.getCalendar();

        //checking if there is no token or if it is expired then creating a new token
        if ((sessionManager.isLoggedIn() && sessionManager.getToken().equals("")) || (!now.before(expire))) {
            getToken();
        }
    }


    public void getToken(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, TOKEN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String token = jsonObject.getString("access_token");
                    int expires_in = jsonObject.getInt("expires_in");
                    sessionManager.addToken(token,expires_in);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                SessionManager sessionManager = new SessionManager(context);
                HashMap<String,String> userDetails = sessionManager.getUserDetails();
                String username = userDetails.get(SessionManager.KEY_USERNAME);
                String hashed_password = userDetails.get(SessionManager.KEY_HASHED_PASSWORD);

                Map<String, String> params = new HashMap<>();
                params.put("client_id",username);
                params.put("client_secret",hashed_password);
                params.put("grant_type","client_credentials");
                return params;
            }
        };
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    //----------------------------------------USER TABLE---------------------------------
    public void addUser(final User user, final DatabaseHelper databaseHelper, final VolleyCallBack callBack){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, USERS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    callBack.onSuccess(Response);
                    if (Response.equals("success")){
                        user.setSync_status(User.SYNC_STATUS_OK);
                        databaseHelper.addUser(user);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    String response = new String(error.networkResponse.data, "UTF-8");
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    callBack.onSuccess(Response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params = new HashMap<>();
                params.put(User.COLUMN_NAME,user.getName());
                params.put(User.COLUMN_USERNAME,user.getUsername());
                params.put(User.COLUMN_PASSWORD,user.getPassword());
                return params;
            }
        };
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    /*
    Checks if users credentials are in webserver.
    If credentials are in webserver, it returns user_name with success message
    AND adds user to a local database.
     */
    public void checkUser(final String username, final String password, final DatabaseHelper databaseHelper, final VolleyCallBack callBack){
        String url = USERS_URL+ "?" + User.COLUMN_USERNAME + "=" + username + "&" + User.COLUMN_PASSWORD + "=" + CryptoHelper.makeHash(password);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String[] Response = jsonObject.getString("response").split(":");
                    callBack.onSuccess(Response[0]);
                    if (Response[0].equals("success")){
                        User user = new User();
                        user.setName(Response[1]);
                        user.setUsername(username);
                        user.setPassword(password);         //hashed in User model
                        user.setSync_status(User.SYNC_STATUS_OK);
                        databaseHelper.addUser(user);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onSuccess("Failed");
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    //-----------------------------------CATEGORY TABLE-------------------------------------
    public void addCategory(final Category category, final VolleyCallBack callBack){
        String url = CATEGORIES_URL + "?access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    int cat_id = jsonObject.getInt("cat_id");
                    callBack.onSuccess(Response + ":" + cat_id);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {

                    addCategory(category, callBack);
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params = new HashMap<>();
                params.put(Category.COLUMN_TITLE,category.getTitle());
                params.put(Category.COLUMN_USERNAME,category.getUsername());
                params.put(Category.COLUMN_PUBLIC_STATUS,Integer.toString(category.getPublic_status()));
                params.put(Category.COLUMN_IMAGE, category.getImage());
                params.put(Category.COLUMN_DESCRIPTION, category.getDescription());
                params.put(Category.COLUMN_SYNC_STATUS, Integer.toString(category.getSync_status()));
                params.put(Category.COLUMN_PASSWORD, category.getPassword());
                params.put(Category.COLUMN_PASSWORD_STATUS, Integer.toString(category.getPassword_status()));
                return params;
            }
        };
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public void updateCategory(final Category category, final VolleyCallBack callBack){
        String url = CATEGORIES_URL + "?access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    callBack.onSuccess(Response);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    updateCategory(category, callBack);
                }
            }
        })
        {
            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                //or try with this:
                //headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params = new HashMap<>();
                params.put(Category.COLUMN_ID, Integer.toString(category.getId()));
                params.put(Category.COLUMN_TITLE,category.getTitle());
                params.put(Category.COLUMN_USERNAME,category.getUsername());
                params.put(Category.COLUMN_PUBLIC_STATUS,Integer.toString(category.getPublic_status()));
                params.put(Category.COLUMN_IMAGE, category.getImage());
                params.put(Category.COLUMN_DESCRIPTION, category.getDescription());
                params.put(Category.COLUMN_SYNC_STATUS, Integer.toString(category.getSync_status()));
                params.put(Category.COLUMN_PASSWORD, category.getPassword());
                params.put(Category.COLUMN_PASSWORD_STATUS, Integer.toString(category.getPassword_status()));
                return params;
            }
        };
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

        public void getAllCategoriesID(final String username, final VolleyCallBack callBack){
        String url = CATEGORIES_URL + "?" + Category.COLUMN_USERNAME + "=" + username + "&access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    if (!Response.equals("null")){
                        callBack.onSuccess(Response);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    getAllCategoriesID(username, callBack);
                }
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public void getCategory(final int category_id, final VolleyCallBack callBack){
        String url = CATEGORIES_URL + "?" + Category.COLUMN_ID + "=" + category_id + "&access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String category_id = jsonObject.getString("category_id");
                    String category_title = jsonObject.getString("category_title");
                    String category_username = jsonObject.getString("category_username");
                    String category_public_status = jsonObject.getString("category_public_status");
                    String category_description = jsonObject.getString("category_description");
                    String category_password = jsonObject.getString("category_password");
                    String category_password_status = jsonObject.getString("category_password_status");
                    String category_image = jsonObject.getString("category_image");


                    String Response = category_id + "<=>" + category_title + "<=>" + category_username + "<=>" + category_public_status +
                            "<=>" + category_description + "<=>" + category_password + "<=>" + category_password_status + "<=>" +
                            category_image;
                    callBack.onSuccess(Response);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    getCategory(category_id, callBack);
                }
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public void deleteCategory(final int category_id, final VolleyCallBack callBack){
        String url = CATEGORIES_URL + "?" + Category.COLUMN_ID + "=" + category_id + "&access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    callBack.onSuccess(Response);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    deleteCategory(category_id, callBack);
                }
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    //-------------------------------QUESTION TABLE---------------------------------------
    public void addQuestion(final Question question, final VolleyCallBack callBack){
        String url = QUESTIONS_URL + "?access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    int quest_id = jsonObject.getInt("quest_id");
                    callBack.onSuccess(Response + ":" + quest_id);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    addQuestion(question, callBack);
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params = new HashMap<>();
                params.put(Question.COLUMN_FK_CATEGORY_ID, Integer.toString(question.getFk_category_id()));
                params.put(Question.COLUMN_CREATOR_USERNAME,question.getCreator_username());
                params.put(Question.COLUMN_OWNER_USERNAME, question.getOwner_username());
                params.put(Question.COLUMN_TYPE,Integer.toString(question.getType()));
                params.put(Question.COLUMN_QUESTION, question.getQuestion());
                params.put(Question.COLUMN_ANSWERS, Arrays.toString(question.getAnswers()));
                params.put(Question.COLUMN_CORRECT_ANSWERS, Arrays.toString(question.getCorrect_answers()));
                params.put(Question.COLUMN_SYNC_STATUS,Integer.toString(question.getSync_status()));
                return params;
            }
        };
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public void getQuestion(final int question_id, final VolleyCallBack callBack){
        String url = QUESTIONS_URL + "?" + Question.COLUMN_ID + "=" + question_id + "&access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    String question_id = jsonObject.getString("question_id");
                    String fk_category_id = jsonObject.getString("fk_category_id");
                    String question_type = jsonObject.getString("question_type");
                    String question_question = jsonObject.getString("question_question");
                    String question_creator_username = jsonObject.getString("question_creator_username");
                    String question_owner_username = jsonObject.getString("question_owner_username");
                    String question_answers = jsonObject.getString("question_answers");
                    String question_correct_answers = jsonObject.getString("question_correct_answers");

                    String Response = question_id + "<=>" + fk_category_id + "<=>" + question_type + "<=>" + question_question +
                            "<=>" + question_creator_username + "<=>" + question_owner_username + "<=>" + question_answers + "<=>" +
                            question_correct_answers;
                    callBack.onSuccess(Response);
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    getQuestion(question_id, callBack);
                }
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public void getAllQuestionsID(final String username, final VolleyCallBack callBack){
        String url = QUESTIONS_URL + "?" + Question.COLUMN_OWNER_USERNAME + "=" + username + "&access_token=" + sessionManager.getToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String Response = jsonObject.getString("response");
                    if (!Response.equals("null")){
                        callBack.onSuccess(Response);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse.statusCode==401) {
                    getAllQuestionsID(username, callBack);
                }
            }
        });
        VolleySingleton.getvInstance(context).addToRequestQueue(stringRequest);
    }

    public interface VolleyCallBack{
        void onSuccess(String result);
    }
}
