package com.example.zpils.victoria.Model;

import com.example.zpils.victoria.Helper.CryptoHelper;

/**
 * Created by zpils on 2017-07-20.
 */

public class Category {
    public static final int SYNC_STATUS_OK = 1;
    public static final int SYNC_STATUS_FAILED = 0;
    public static final int DEFAULT_ID = -1;
    public static final String TABLE_CATEGORIES = "categories";
    public static final String COLUMN_ID = "category_id";
    public static final String COLUMN_TITLE = "category_title";
    public static final String COLUMN_USERNAME = "category_username";
    public static final String COLUMN_PUBLIC_STATUS = "category_public_status";
    public static final String COLUMN_IMAGE = "category_image";
    public static final String COLUMN_DESCRIPTION = "category_description";
    public static final String COLUMN_SYNC_STATUS = "category_sync_status";
    public static final String COLUMN_PASSWORD = "category_password";
    public static final String COLUMN_PASSWORD_STATUS = "category_password_status";

    public static final String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_TITLE + " TEXT,"
            + COLUMN_USERNAME + " TEXT,"
            + COLUMN_PUBLIC_STATUS + " INTEGER,"
            + COLUMN_DESCRIPTION + " TEXT,"
            + COLUMN_SYNC_STATUS + " INTEGER,"
            + COLUMN_PASSWORD + " TEXT,"
            + COLUMN_PASSWORD_STATUS + " INTEGER,"
            + COLUMN_IMAGE + " TEXT)";
    public static final String DROP_CATEGORIES_TABLE = "DROP TABLE IF EXISTS" + TABLE_CATEGORIES;


    private boolean password_hashed;

    private int id;
    private String title;
    private String username;
    private int public_status;
    private String image;
    private String description;
    private int sync_status;
    private String password;
    private int password_status;


    public Category(boolean password_hashed){
        this.password_hashed = password_hashed;
    }

    public Category(){
        this.password_hashed = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPublic_status() {
        return public_status;
    }

    public void setPublic_status(int public_status) {
        this.public_status = public_status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSync_status() {
        return sync_status;
    }

    public void setSync_status(int sync_status) {
        this.sync_status = sync_status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.isEmpty() || password_hashed) {
            this.password = password;
        } else {
            this.password = CryptoHelper.makeHash(password);
            password_hashed = true;
        }
    }

    public int getPassword_status() {
        return password_status;
    }

    public void setPassword_status(int password_status) {
        this.password_status = password_status;
    }
}
