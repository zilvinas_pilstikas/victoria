package com.example.zpils.victoria.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.InputValidation;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.Model.Category;
import com.example.zpils.victoria.R;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by zpils on 2017-07-21.
 */


/**
 * There might be some error because of unique category id.
 * Error occurs when there are no data(but not neccessarily) in AppServer and in DatabaseServer and app is used by few accounts on the same device.
 * When 1st user enters 5 new categories with connection, than enters 5 new categories without connection,
 * logouts and 2nd user logins and tries to enter 5 new categories with connection - error occurs.
 * Because in server 1-5 ID are taken(by 1st user), 1-10 ID are taken in local (by 1st user)
 * so when 2nd user tries to enter new category online, first it saves in server with ID 6 and
 * then tries to save it to local database with ID 6 which is already taken
 */

/**
 * Still needs field validation, make some default pictures
 */
public class NewCategoryActivity extends AppCompatActivity implements View.OnClickListener{

    private final int IMG_REQUEST = 1;
    private Bitmap bitmap;
    private final AppCompatActivity activity = NewCategoryActivity.this;
    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutTitle;
    private TextInputLayout textInputLayoutDescription;
    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmPassword;

    private TextInputEditText textInputEditTextTitle;
    private TextInputEditText textInputEditTextDescription;
    private TextInputEditText textInputEditTextPassword;
    private TextInputEditText textInputEditTextConfirmPassword;

    private AppCompatButton appCompatButtonAddCategory;
    private AppCompatImageButton appCompatImageButtonUpload;

    private Switch switchPublicAccess;
    private Switch switchPasswordProtected;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private Category category;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_category);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutTitle = (TextInputLayout) findViewById(R.id.textInputLayoutTitle);
        textInputLayoutDescription = (TextInputLayout) findViewById(R.id.textInputLayoutDescription);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        textInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);

        textInputEditTextTitle = (TextInputEditText) findViewById(R.id.textInputEditTextTitle);
        textInputEditTextDescription = (TextInputEditText) findViewById(R.id.textInputEditTextDescription);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
        textInputEditTextConfirmPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);

        appCompatButtonAddCategory = (AppCompatButton) findViewById(R.id.appCompatButtonAddCategory);
        appCompatImageButtonUpload = (AppCompatImageButton) findViewById(R.id.appCompatImageButtonUpload);

        switchPublicAccess = (Switch) findViewById(R.id.switchPublicAccess);
        switchPasswordProtected = (Switch) findViewById(R.id.switchPasswordProtected);
    }

    private void initListeners(){
        appCompatButtonAddCategory.setOnClickListener(this);
        appCompatImageButtonUpload.setOnClickListener(this);
        switchPasswordProtected.setOnClickListener(this);
    }

    private void initObjects(){
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        appServerHelper = new AppServerHelper(activity);
        category = new Category();
        sessionManager = new SessionManager(activity);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.appCompatButtonAddCategory:
                addCategory();
                break;
            case R.id.appCompatImageButtonUpload:
                selectImage();
                break;
            case R.id.switchPasswordProtected:

                //enable/disable password fields
                if (switchPasswordProtected.isChecked()){
                    textInputLayoutPassword.setVisibility(View.VISIBLE);
                    textInputLayoutConfirmPassword.setVisibility(View.VISIBLE);
                } else if (!switchPasswordProtected.isChecked()){
                    textInputLayoutPassword.setVisibility(View.GONE);
                    textInputLayoutConfirmPassword.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void selectImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null){
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                appCompatImageButtonUpload.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addCategory(){
        HashMap<String,String> userDetails = sessionManager.getUserDetails();

        if (!checkValues(userDetails)){
            return;
        }

        category.setTitle(textInputEditTextTitle.getText().toString().trim());
        category.setUsername(userDetails.get(SessionManager.KEY_USERNAME));
        category.setPublic_status(CryptoHelper.booleanToInteger(switchPublicAccess.isChecked()));
        category.setDescription(textInputEditTextDescription.getText().toString().trim());
        category.setSync_status(Category.SYNC_STATUS_FAILED);
        category.setPassword(textInputEditTextPassword.getText().toString().trim());
        category.setPassword_status(CryptoHelper.booleanToInteger(switchPasswordProtected.isChecked()));
        category.setImage(CryptoHelper.imageToString(bitmap));

        if (NetworkMonitor.checkNetworkConnection(activity)) {
            category.setSync_status(Category.SYNC_STATUS_OK);
            appServerHelper.addCategory(category, new AppServerHelper.VolleyCallBack() {
                @Override
                public void onSuccess(String result) {
                    if (result.contains("success")) {
                        //adds category to local database
                        int cat_id = Integer.parseInt(result.split(":")[1]);
                        databaseHelper.addCategory(category,cat_id);

                        //Snack Bar to show success message that record saved successfully
                        Snackbar.make(nestedScrollView, getString(R.string.category_create_success), Snackbar.LENGTH_LONG).show();
                        Intent usersIntent = new Intent(activity, UsersActivity.class);
                        emptyInputEditText();
                        startActivity(usersIntent);
                        finish();
                    } else {
                        //Snack Bar to show error message that something went wrong
                        Snackbar.make(nestedScrollView, getString(R.string.category_create_faill), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            category.setSync_status(Category.SYNC_STATUS_FAILED);
            long rowID = databaseHelper.addCategory(category,Category.DEFAULT_ID);

            if (rowID == -1) {
                //Snack Bar to show error message that something went wrong
                Snackbar.make(nestedScrollView, getString(R.string.category_create_faill), Snackbar.LENGTH_LONG).show();
            } else {
                //Snack Bar to show success message that record saved successfully
                Snackbar.make(nestedScrollView, getString(R.string.category_create_success), Snackbar.LENGTH_LONG).show();
                Intent usersIntent = new Intent(activity, UsersActivity.class);
                emptyInputEditText();
                startActivity(usersIntent);
                finish();

            }
        }
    }

    private void emptyInputEditText(){
        textInputEditTextTitle.setText(null);
        textInputEditTextDescription.setText(null);
        textInputEditTextPassword.setText(null);
        textInputEditTextConfirmPassword.setText(null);
        switchPublicAccess.setChecked(false);
        switchPasswordProtected.setChecked(false);
    }

    private boolean checkValues(HashMap<String,String> userDetails) {
        //Title is empty
        if (!inputValidation.isInputEditTextFilled(textInputEditTextTitle,textInputLayoutTitle,getString(R.string.error_message_category_title))){
            return false;
        }

        //Description is empty
        if (!inputValidation.isInputEditTextFilled(textInputEditTextDescription,textInputLayoutDescription,getString(R.string.error_message_category_description))){
            return false;
        }

        //password protected && (empty || not match)
        if (switchPasswordProtected.isChecked()){
            if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword,textInputLayoutPassword,getString(R.string.error_message_category_password))){
                return false;
            } else if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword,textInputEditTextConfirmPassword,textInputLayoutPassword,getString(R.string.error_password_match))){
                return false;
            }
        }

        //user details are empty
        if (userDetails.isEmpty()){
            Snackbar.make(nestedScrollView,getString(R.string.error_user_details_not_found),Snackbar.LENGTH_SHORT).show();
            return false;
        }

        //picture not found
        if (bitmap == null){
            Snackbar.make(nestedScrollView,getString(R.string.error_message_category_image), Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
