package com.example.zpils.victoria.Model;

import com.example.zpils.victoria.Helper.CryptoHelper;

/**
 * Created by zpils on 2017-06-16.
 */

public class User {

    public static final int SYNC_STATUS_OK = 1;
    public static final int SYNC_STATUS_FAILED = 0;
    public static final String TABLE_USERS = "users";
    public static final String COLUMN_ID = "user_id";
    public static final String COLUMN_NAME = "user_name";
    public static final String COLUMN_USERNAME = "user_username";
    public static final String COLUMN_PASSWORD = "user_password";
    public static final String COLUMN_SYNC_STATUS = "user_sync_status";
    public static final String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + " TEXT,"
            + COLUMN_USERNAME + " TEXT,"
            + COLUMN_PASSWORD + " TEXT,"
            + COLUMN_SYNC_STATUS + " INTEGER)";
    public static final String DROP_USERS_TABLE = "DROP TABLE IF EXISTS" + TABLE_USERS;


    private int id;
    private String name;
    private String username;
    private String password;
    private int sync_status;

    /*
    Setters
     */
    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = CryptoHelper.makeHash(password);
    }

    public void setSync_status(int sync_status) { this.sync_status = sync_status; }

    /*
    Getters
     */
    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public int getSync_status() { return sync_status; }
}
