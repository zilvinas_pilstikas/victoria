package com.example.zpils.victoria.Activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.InputValidation;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Model.User;
import com.example.zpils.victoria.R;

/**
 * Created by zpils on 2017-06-16.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private final AppCompatActivity activity = RegisterActivity.this;

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutUsername;
    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmPassword;

    private TextInputEditText textInputEditTextName;
    private TextInputEditText textInputEditTextUsername;
    private TextInputEditText textInputEditTextPassword;
    private TextInputEditText textInputEditTextConfirmPassword;

    private AppCompatButton appCompatButtonRegister;
    private AppCompatTextView appCompatTextViewLoginLink;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutUsername = (TextInputLayout) findViewById(R.id.textInputLayoutUsername);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        textInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);

        textInputEditTextName = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextUsername = (TextInputEditText) findViewById(R.id.textInputEditTextUsername);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
        textInputEditTextConfirmPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);

        appCompatButtonRegister = (AppCompatButton) findViewById(R.id.appCompatButtonRegister);

        appCompatTextViewLoginLink = (AppCompatTextView) findViewById(R.id.appCompatTextViewLoginLink);
    }

    private void initListeners(){
        appCompatButtonRegister.setOnClickListener(this);
        appCompatTextViewLoginLink.setOnClickListener(this);
    }

    private void initObjects(){
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        appServerHelper = new AppServerHelper(activity);
        user = new User();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.appCompatButtonRegister:
                registerUser();
                break;
            case R.id.appCompatTextViewLoginLink:
                finish();
                break;
        }
    }

    /*
    Registers new user to AppServer and SQLite database,
    IF internet connection is available AND username is unique
     */
    private void registerUser(){
        if (NetworkMonitor.checkNetworkConnection(activity)) {
            if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_name))) {
                return;
            }
            if (!inputValidation.isInputEditTextFilled(textInputEditTextUsername, textInputLayoutUsername, getString(R.string.error_message_username))) {
                return;
            }
            if (!inputValidation.isInputEditTextUsername(textInputEditTextUsername, textInputLayoutUsername, getString(R.string.error_message_username))) {
                return;
            }
            if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
                return;
            }
            if (!inputValidation.isInputEditTextMatches(textInputEditTextPassword, textInputEditTextConfirmPassword,
                    textInputLayoutConfirmPassword, getString(R.string.error_password_match))) {
                return;
            }
            user.setName(textInputEditTextName.getText().toString().trim());
            user.setUsername(textInputEditTextUsername.getText().toString().trim());
            user.setPassword(textInputEditTextPassword.getText().toString().trim());
            saveUser(user);
        } else {
            //Snack Bar to show error message that Internet connection is required
            Snackbar.make(nestedScrollView, getString(R.string.internet_connection_required), Snackbar.LENGTH_LONG).show();
        }
    }

    /*
    Save user to AppServer and SQLite database
     */
    private void saveUser(User user){
        appServerHelper.addUser(user, databaseHelper, new AppServerHelper.VolleyCallBack() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("success")){

                    //Snack Bar to show success message that record saved successfully
                    Snackbar.make(nestedScrollView, getString(R.string.success_message), Snackbar.LENGTH_LONG).show();
                    emptyInputEditText();
                } else if (result.equals("bad_username")){
                    //Snack Bar to show error message that record already exists
                    Snackbar.make(nestedScrollView, getString(R.string.error_username_exists), Snackbar.LENGTH_LONG).show();
                } else {
                    //Snack Bar to show error message that something went wrong
                    Snackbar.make(nestedScrollView, getString(R.string.registration_failed), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void emptyInputEditText(){
        textInputEditTextName.setText(null);
        textInputEditTextUsername.setText(null);
        textInputEditTextPassword.setText(null);
        textInputEditTextConfirmPassword.setText(null);
    }
}
