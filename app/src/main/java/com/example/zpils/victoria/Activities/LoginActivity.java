package com.example.zpils.victoria.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.NetworkOnMainThreadException;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.InputValidation;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.R;

import java.io.ObjectInputValidation;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = LoginActivity.this;
    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutUsername;
    private TextInputLayout textInputLayoutPassword;

    private TextInputEditText textInputEditTextUsername;
    private TextInputEditText textInputEditTextPassword;

    private AppCompatButton appCompatButtonLogin;
    private AppCompatTextView textViewLinkRegister;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    private void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutUsername = (TextInputLayout) findViewById(R.id.textInputLayoutUsername);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        textInputEditTextUsername = (TextInputEditText) findViewById(R.id.textInputEditTextUsername);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);

        appCompatButtonLogin = (AppCompatButton) findViewById(R.id.appCompatButtonLogin);

        textViewLinkRegister = (AppCompatTextView) findViewById(R.id.textViewLinkRegister);
    }

    private void initListeners(){
        appCompatButtonLogin.setOnClickListener(this);
        textViewLinkRegister.setOnClickListener(this);
    }

    private void initObjects(){
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);
        appServerHelper = new AppServerHelper(activity);
        sessionManager = new SessionManager(activity);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.appCompatButtonLogin:
                verifyFromSQLite();
                break;
            case R.id.textViewLinkRegister:
                Intent intentRegister = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intentRegister);
                break;
        }
    }

    private void verifyFromSQLite(){
        if (!inputValidation.isInputEditTextFilled(textInputEditTextUsername,textInputLayoutUsername,getString(R.string.error_message_username))){
            return;
        }
        if (!inputValidation.isInputEditTextUsername(textInputEditTextUsername, textInputLayoutUsername, getString(R.string.error_message_username))){
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword,textInputLayoutPassword, getString(R.string.error_message_password))){
            return;
        }
        final String username = textInputEditTextUsername.getText().toString().trim();
        final String password = textInputEditTextPassword.getText().toString().trim();

        if (databaseHelper.checkUser(username,password)){
            Intent accountsIntent = new Intent(activity, UsersActivity.class);
            sessionManager.createLoginSession(username,CryptoHelper.makeHash(password));
            emptyInputEditText();
            startActivity(accountsIntent);
            finish();
        } else if (NetworkMonitor.checkNetworkConnection(activity)) {
            appServerHelper.checkUser(username, password, databaseHelper, new AppServerHelper.VolleyCallBack() {
                @Override
                public void onSuccess(String result) {
                    if (result.equals("success")){
                        Intent accountsIntent = new Intent(activity, UsersActivity.class);
                        sessionManager.createLoginSession(username,CryptoHelper.makeHash(password));
                        emptyInputEditText();
                        startActivity(accountsIntent);
                        finish();
                    } else {
                        Snackbar.make(nestedScrollView, getString(R.string.error_valid_username_password), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            Snackbar.make(nestedScrollView, getString(R.string.error_valid_username_password_connection_required), 4500).show();
        }
    }

    private void emptyInputEditText(){
        textInputEditTextUsername.setText(null);
        textInputEditTextPassword.setText(null);
    }
}


