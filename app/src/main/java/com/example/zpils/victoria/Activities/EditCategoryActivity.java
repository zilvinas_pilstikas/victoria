package com.example.zpils.victoria.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Switch;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Helper.InputValidation;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.Model.Category;
import com.example.zpils.victoria.R;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by zpils on 2017-09-22.
 */

public class EditCategoryActivity extends AppCompatActivity implements View.OnClickListener {
    private final int IMG_REQUEST = 1;
    private Bitmap bitmap;
    private final AppCompatActivity activity = EditCategoryActivity.this;
    private NestedScrollView nestedScrollView;

    private AppCompatTextView appCompatTextViewEditCategory;

    private TextInputLayout textInputLayoutPassword;
    private TextInputLayout textInputLayoutConfirmPassword;
    private TextInputLayout textInputLayoutTitle;
    private TextInputLayout textInputLayoutDescription;

    private TextInputEditText textInputEditTextTitle;
    private TextInputEditText textInputEditTextDescription;
    private TextInputEditText textInputEditTextPassword;
    private TextInputEditText textInputEditTextConfirmPassword;

    private AppCompatButton appCompatButtonUpdateCategory;
    private AppCompatImageButton appCompatImageButtonUpload;

    private Switch switchPublicAccess;
    private Switch switchPasswordProtected;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private Category category;
    private SessionManager sessionManager;

    private int category_id;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_category);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
        populateFields();
    }

    private void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        appCompatTextViewEditCategory = (AppCompatTextView) findViewById(R.id.appCompatTextViewEditCategory);
        textInputLayoutTitle = (TextInputLayout) findViewById(R.id.textInputLayoutTitle);
        textInputLayoutDescription = (TextInputLayout) findViewById(R.id.textInputLayoutDescription);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        textInputLayoutConfirmPassword = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmPassword);

        textInputEditTextTitle = (TextInputEditText) findViewById(R.id.textInputEditTextTitle);
        textInputEditTextDescription = (TextInputEditText) findViewById(R.id.textInputEditTextDescription);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);
        textInputEditTextConfirmPassword = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmPassword);

        appCompatButtonUpdateCategory = (AppCompatButton) findViewById(R.id.appCompatButtonUpdateCategory);
        appCompatImageButtonUpload = (AppCompatImageButton) findViewById(R.id.appCompatImageButtonUpload);

        switchPublicAccess = (Switch) findViewById(R.id.switchPublicAccess);
        switchPasswordProtected = (Switch) findViewById(R.id.switchPasswordProtected);
    }

    private void initListeners(){
        appCompatButtonUpdateCategory.setOnClickListener(this);
        appCompatImageButtonUpload.setOnClickListener(this);
        switchPasswordProtected.setOnClickListener(this);
    }

    private void initObjects(){
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        appServerHelper = new AppServerHelper(activity);
        category = new Category();
        sessionManager = new SessionManager(activity);

        category_id = getIntent().getIntExtra("category_id", -1);
        System.out.println(category_id);
    }

    private void populateFields() {
        category = databaseHelper.getCategory(category_id);

        appCompatTextViewEditCategory.setText(category.getTitle());
        textInputEditTextTitle.setText(category.getTitle());
        textInputEditTextDescription.setText(category.getDescription());
        switchPublicAccess.setChecked(category.getPublic_status()==1);
        switchPasswordProtected.setChecked(category.getPassword_status()==1);
        textInputLayoutPassword.setVisibility(switchPasswordProtected.isChecked() ? View.VISIBLE : View.GONE);
        textInputLayoutConfirmPassword.setVisibility(switchPasswordProtected.isChecked() ? View.VISIBLE : View.GONE);
        bitmap = CryptoHelper.StringToBitMap(category.getImage());
        appCompatImageButtonUpload.setImageBitmap(bitmap);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.appCompatButtonUpdateCategory:
                updateCategory();
                break;
            case R.id.appCompatImageButtonUpload:
                selectImage();
                break;
            case R.id.switchPasswordProtected:

                //enable/disable password fields
                if (switchPasswordProtected.isChecked()){
                    textInputLayoutPassword.setVisibility(View.VISIBLE);
                    textInputLayoutConfirmPassword.setVisibility(View.VISIBLE);
                } else if (!switchPasswordProtected.isChecked()){
                    textInputLayoutPassword.setVisibility(View.GONE);
                    textInputLayoutConfirmPassword.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void selectImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null){
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                appCompatImageButtonUpload.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCategory(){

        HashMap<String,String> UserDetails = sessionManager.getUserDetails();

        category.setTitle(textInputEditTextTitle.getText().toString().trim());
        category.setUsername(UserDetails.get(SessionManager.KEY_USERNAME));
        category.setPublic_status(CryptoHelper.booleanToInteger(switchPublicAccess.isChecked()));
        category.setDescription(textInputEditTextDescription.getText().toString().trim());
        category.setSync_status(Category.SYNC_STATUS_FAILED);
        category.setPassword(textInputEditTextPassword.getText().toString().trim());
        category.setPassword_status(CryptoHelper.booleanToInteger(switchPasswordProtected.isChecked()));
        category.setImage(CryptoHelper.imageToString(bitmap));

        if (NetworkMonitor.checkNetworkConnection(activity)) {
            category.setSync_status(Category.SYNC_STATUS_OK);
            appServerHelper.updateCategory(category, new AppServerHelper.VolleyCallBack() {
                @Override
                public void onSuccess(String result) {
                    if (result.contains("success")) {
                        //updates category in local database
                        databaseHelper.updateCategory(category);

                        //Snack Bar to show success message that record updated successfully
                        Snackbar.make(nestedScrollView, getString(R.string.category_update_success), Snackbar.LENGTH_LONG).show();
                        Intent usersIntent = new Intent(activity, UsersActivity.class);
                        emptyInputEditText();
                        startActivity(usersIntent);
                        finish();
                    } else {
                        //Snack Bar to show error message that something went wrong
                        Snackbar.make(nestedScrollView, getString(R.string.category_update_fail), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        } else {
                //Snack Bar to show error message that internet connection required
                Snackbar.make(nestedScrollView, getString(R.string.internet_connection_required), Snackbar.LENGTH_LONG).show();
        }
    }

    private void emptyInputEditText(){
        textInputEditTextTitle.setText(null);
        textInputEditTextDescription.setText(null);
        textInputEditTextPassword.setText(null);
        textInputEditTextConfirmPassword.setText(null);
        switchPublicAccess.setChecked(false);
        switchPasswordProtected.setChecked(false);
    }
}
