package com.example.zpils.victoria.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.format.DateUtils;

import com.example.zpils.victoria.Activities.LoginActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by zpils on 2017-07-12.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AndroiVictoriaPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_HASHED_PASSWORD = "password";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_TOKEN_EXPIRES = "expires";

    // Constructor
    public SessionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String username, String hashed_password){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_HASHED_PASSWORD, hashed_password);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            Intent i = new Intent(context, LoginActivity.class);
            context.startActivity(i);
        }
    }

    public void addToken(String token, int expires_in_seconds){
        editor.putString(KEY_TOKEN,token);

        editor.putString(KEY_TOKEN_EXPIRES, getExpire(expires_in_seconds));
        editor.commit();
    }

    public String getToken(){
        return pref.getString(KEY_TOKEN,"");
    }

    private String getExpire(int expires_in_seconds){

        Calendar cal = Calendar.getInstance();
        DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        cal.add(Calendar.SECOND, expires_in_seconds);
        return sdf.format(cal.getTime()).toString();
    }

    public String getExpireDate(){
        return pref.getString(KEY_TOKEN_EXPIRES,"");
    }
    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_HASHED_PASSWORD, pref.getString(KEY_HASHED_PASSWORD, null));
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(context, LoginActivity.class);
        context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
