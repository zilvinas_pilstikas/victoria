package com.example.zpils.victoria.Helper;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by zpils on 2017-07-01.
 */

public class VolleySingleton {
    private static VolleySingleton vInstance;
    private RequestQueue requestQueue;
    private static Context vContext;

    private VolleySingleton(Context context){
        vContext = context;
        requestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue(){
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(vContext.getApplicationContext());
        return requestQueue;
    }

    public static synchronized VolleySingleton getvInstance(Context context){
        if (vInstance == null){
            vInstance = new VolleySingleton(context);
        }
        return vInstance;
    }

    public<T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
