package com.example.zpils.victoria.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.SyncStateContract;

import com.example.zpils.victoria.Helper.CryptoHelper;
import com.example.zpils.victoria.Model.Category;
import com.example.zpils.victoria.Model.Question;
import com.example.zpils.victoria.Model.User;

import java.util.Arrays;

/**
 * Created by zpils on 2017-06-16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Victoria.db";

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(User.CREATE_USERS_TABLE);
        db.execSQL(Category.CREATE_CATEGORIES_TABLE);
        db.execSQL(Question.CREATE_QUESTIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(User.DROP_USERS_TABLE);
        db.execSQL(Category.DROP_CATEGORIES_TABLE);
        db.execSQL(Question.DROP_QUESTIONS_TABLE);

        onCreate(db);
    }

    //-----------------------------------USER TABLE---------------------------------------//

    public void addUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_NAME,user.getName());
        values.put(User.COLUMN_USERNAME,user.getUsername());
        values.put(User.COLUMN_PASSWORD,user.getPassword());
        values.put(User.COLUMN_SYNC_STATUS, user.getSync_status());

        db.insert(User.TABLE_USERS,null,values);
        db.close();
    }

    public void updateUserSyncStatus(String username, int sync_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_SYNC_STATUS, sync_status);
        String selection = User.COLUMN_USERNAME + " LIKE ?";
        String[] selection_args = {username };
        db.update(User.TABLE_USERS,values,selection,selection_args);
        db.close();
    }

    public boolean checkUser(String username){
        String[] columns = {
                User.COLUMN_ID
        };
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = User.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { username };

        Cursor cursor = db.query(User.TABLE_USERS, columns, selection,selectionArgs,null,null,null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0){
            return true;
        }
        return false;
    }

    public boolean checkUser(String username,String password){
        String[] columns = {
                User.COLUMN_ID
        };
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = User.COLUMN_USERNAME + " = ?" + " AND " + User.COLUMN_PASSWORD + " = ?";
        String[] selectionArgs = { username, CryptoHelper.makeHash(password) };
        Cursor cursor = db.query(User.TABLE_USERS,columns,selection,selectionArgs,null,null,null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0){
            return true;
        }

        return false;
    }

    public Cursor getAllUsers(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(User.TABLE_USERS,null,null,null,null,null,null);
        db.close();
        return cursor;
    }

    //-----------------------------------CATAGORY TABLE---------------------------------------//

    public long addCategory(Category category,int cat_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Category.COLUMN_TITLE,category.getTitle());
        values.put(Category.COLUMN_USERNAME,category.getUsername());
        values.put(Category.COLUMN_PUBLIC_STATUS,Integer.toString(category.getPublic_status()));
        values.put(Category.COLUMN_IMAGE, category.getImage());
        values.put(Category.COLUMN_DESCRIPTION, category.getDescription());
        values.put(Category.COLUMN_SYNC_STATUS, Integer.toString(category.getSync_status()));
        values.put(Category.COLUMN_PASSWORD, category.getPassword());
        values.put(Category.COLUMN_PASSWORD_STATUS, Integer.toString(category.getPassword_status()));
        if (cat_id != Category.DEFAULT_ID) {
            values.put(Category.COLUMN_ID, cat_id);
        }

        long rowID = db.insert(Category.TABLE_CATEGORIES,null,values);
        db.close();
        return rowID;
    }

    public Category getCategory(int cat_id){
        String[] columns = {
                Category.COLUMN_TITLE,
                Category.COLUMN_IMAGE,
                Category.COLUMN_ID,
                Category.COLUMN_DESCRIPTION,
                Category.COLUMN_PASSWORD_STATUS,
                Category.COLUMN_PUBLIC_STATUS,
                Category.COLUMN_USERNAME,
                Category.COLUMN_SYNC_STATUS
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Category.COLUMN_ID + " = ?";
        String[] selectionArgs = { Integer.toString(cat_id)};
        Cursor cursor;
        try {
            cursor = db.query(Category.TABLE_CATEGORIES, columns, selection, selectionArgs, null, null, null);
            Category category = new Category();
            if (cursor.moveToFirst()){
                String title = cursor.getString(cursor.getColumnIndex(Category.COLUMN_TITLE));
                String image = cursor.getString(cursor.getColumnIndex(Category.COLUMN_IMAGE));
                String descp = cursor.getString(cursor.getColumnIndex(Category.COLUMN_DESCRIPTION));
                int pass_status = cursor.getInt(cursor.getColumnIndex(Category.COLUMN_PASSWORD_STATUS));
                int publ_status = cursor.getInt(cursor.getColumnIndex(Category.COLUMN_PUBLIC_STATUS));
                int sync_status = cursor.getInt(cursor.getColumnIndex(Category.COLUMN_SYNC_STATUS));
                String username = cursor.getString(cursor.getColumnIndex(Category.COLUMN_USERNAME));

                category.setTitle(title);
                category.setImage(image);
                category.setDescription(descp);
                category.setPassword("");
                category.setPassword_status(pass_status);
                category.setPublic_status(publ_status);
                category.setSync_status(sync_status);
                category.setUsername(username);
                category.setId(cat_id);
            }
            return category;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    public void updateCategory(Category category){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Category.COLUMN_TITLE,category.getTitle());
        values.put(Category.COLUMN_USERNAME,category.getUsername());
        values.put(Category.COLUMN_PUBLIC_STATUS,Integer.toString(category.getPublic_status()));
        values.put(Category.COLUMN_IMAGE, category.getImage());
        values.put(Category.COLUMN_DESCRIPTION, category.getDescription());
        values.put(Category.COLUMN_SYNC_STATUS, Integer.toString(category.getSync_status()));
        values.put(Category.COLUMN_PASSWORD, category.getPassword());
        values.put(Category.COLUMN_PASSWORD_STATUS, Integer.toString(category.getPassword_status()));
        String selection = Category.COLUMN_ID + " = ?";
        String[] selection_args = { Integer.toString(category.getId()) };
        db.update(Category.TABLE_CATEGORIES,values,selection,selection_args);
        db.close();
    }

    public void deleteCategory(int category_id){
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = Category.COLUMN_ID + " = ?";
        String[] selection_args = {Integer.toString(category_id) };
        db.delete(Category.TABLE_CATEGORIES,selection,selection_args);
        db.close();
    }

    public Cursor getAllCategories(String username){
        String[] columns = {
                Category.COLUMN_TITLE,
                Category.COLUMN_IMAGE,
                Category.COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Category.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { username};
        Cursor cursor;
            try {
                cursor = db.query(Category.TABLE_CATEGORIES, columns, selection, selectionArgs, null, null, null);
                return cursor;
            } catch (Exception e){
                e.printStackTrace();
        }
        db.close();
        return null;
    }

    public Cursor getAllUnsynchronizedCategories(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Category.COLUMN_USERNAME + " = ?" + " AND " + Category.COLUMN_SYNC_STATUS + " = " + Category.SYNC_STATUS_FAILED;
        String[] selectionArgs = { username};
        Cursor cursor;
        try {
            cursor = db.query(Category.TABLE_CATEGORIES, null, selection, selectionArgs, null, null, null);
            return cursor;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    public void updateCategorySyncStatusAndID(int sync_status, int old_id, int new_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Category.COLUMN_SYNC_STATUS, sync_status);
        values.put(Category.COLUMN_ID, new_id);
        String selection = Category.COLUMN_ID + " = ?";
        String[] selection_args = {Integer.toString(old_id) };
        db.update(Category.TABLE_CATEGORIES,values,selection,selection_args);
        db.close();
    }

    public int[] getAllCategoriesID(String username){
        String[] columns = {
                Category.COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Category.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { username};
        Cursor cursor;
        try {
            cursor = db.query(Category.TABLE_CATEGORIES, columns, selection, selectionArgs, null, null, null);
            int[] ids = new int[cursor.getCount()];
            int i = 0;
            if (cursor.moveToFirst()){
                ids = new int[cursor.getCount()];
                do {
                    ids[i] = cursor.getInt(cursor.getColumnIndex(Category.COLUMN_ID));
                    i++;
                } while (cursor.moveToNext());
            }
            return ids;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    //-----------------------------------QUESTION TABLE---------------------------------------//

    public long addQuestion(Question question,int quest_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Question.COLUMN_FK_CATEGORY_ID, Integer.toString(question.getFk_category_id()));
        values.put(Question.COLUMN_TYPE, question.getType());
        values.put(Question.COLUMN_QUESTION, question.getQuestion());
        values.put(Question.COLUMN_CREATOR_USERNAME, question.getCreator_username());
        values.put(Question.COLUMN_OWNER_USERNAME, question.getOwner_username());
        values.put(Question.COLUMN_SYNC_STATUS, question.getSync_status());
        values.put(Question.COLUMN_ANSWERS, Arrays.toString(question.getAnswers()));
        values.put(Question.COLUMN_CORRECT_ANSWERS, Arrays.toString(question.getCorrect_answers()));
        if (quest_id != Question.DEFAULT_ID) {
            values.put(Question.COLUMN_ID, quest_id);
        }

        long rowID = db.insert(Question.TABLE_QUESTIONS,null,values);
        db.close();
        return rowID;
    }

    public int[] getQuestionIDs(int cat_id){
        String[] columns = {
                Question.COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Question.COLUMN_FK_CATEGORY_ID + " = ?";
        String[] selectionArgs = { Integer.toString(cat_id) };
        Cursor cursor;
        try {
            cursor = db.query(Question.TABLE_QUESTIONS, columns, selection, selectionArgs, null, null, null);
            int ids[] = new int[cursor.getCount()];
            int i = 0;
            if (cursor.moveToFirst()){
                do {
                    ids[i] = cursor.getInt(cursor.getColumnIndex(Question.COLUMN_ID));
                    i++;
                } while (cursor.moveToNext());
            }
            return ids;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    public Question getQuestion(int cat_id, int quest_id){
        String[] columns = {
                Question.COLUMN_TYPE,
                Question.COLUMN_QUESTION,
                Question.COLUMN_ANSWERS,
                Question.COLUMN_CORRECT_ANSWERS
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Question.COLUMN_FK_CATEGORY_ID + " = ?" + " AND " + Question.COLUMN_ID + " = ?";
        String[] selectionArgs = { Integer.toString(cat_id), Integer.toString(quest_id) };
        Cursor cursor;
        try {
            cursor = db.query(Question.TABLE_QUESTIONS, columns, selection, selectionArgs, null, null, null);
            Question question = new Question();
            if (cursor.moveToFirst()){
                String answers = cursor.getString(cursor.getColumnIndex(Question.COLUMN_ANSWERS));
                String[] ansArray = answers.substring(1,answers.length()-1).split(",");
                String correct_answers = cursor.getString(cursor.getColumnIndex(Question.COLUMN_CORRECT_ANSWERS));
                String[] correct_ansArray = correct_answers.substring(1,correct_answers.length()-1).split(",");

                question.setType(cursor.getInt(cursor.getColumnIndex(Question.COLUMN_TYPE)));
                question.setQuestion(cursor.getString(cursor.getColumnIndex(Question.COLUMN_QUESTION)));
                question.setAnswers(ansArray);
                question.setCorrect_answers(correct_ansArray);
            }
            return question;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    public Cursor getAllUnsynchronizedQuestions(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Question.COLUMN_OWNER_USERNAME + " = ?" + " AND " + Question.COLUMN_SYNC_STATUS + " = " + Question.SYNC_STATUS_FAILED;
        String[] selectionArgs = { username};
        Cursor cursor;
        try {
            cursor = db.query(Question.TABLE_QUESTIONS, null, selection, selectionArgs, null, null, null);
            return cursor;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }

    public void updateQuestionSyncStatusAndID(int sync_status, int old_id, int new_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Question.COLUMN_SYNC_STATUS, sync_status);
        values.put(Question.COLUMN_ID, new_id);
        String selection = Question.COLUMN_ID + " = ?";
        String[] selection_args = {Integer.toString(old_id) };
        db.update(Question.TABLE_QUESTIONS,values,selection,selection_args);
        db.close();
    }

    public int[] getAllQuestionsID(String username){
        String[] columns = {
                Question.COLUMN_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = Question.COLUMN_OWNER_USERNAME + " = ?";
        String[] selectionArgs = { username};
        Cursor cursor;
        try {
            cursor = db.query(Question.TABLE_QUESTIONS, columns, selection, selectionArgs, null, null, null);
            int[] ids = new int[cursor.getCount()];
            int i = 0;
            if (cursor.moveToFirst()){
                ids = new int[cursor.getCount()];
                do {
                    ids[i] = cursor.getInt(cursor.getColumnIndex(Question.COLUMN_ID));
                    i++;
                } while (cursor.moveToNext());
            }
            return ids;
        } catch (Exception e){
            e.printStackTrace();
        }
        db.close();
        return null;
    }
}