package com.example.zpils.victoria.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.zpils.victoria.Database.AppServerHelper;
import com.example.zpils.victoria.Database.DatabaseHelper;
import com.example.zpils.victoria.Helper.InputValidation;
import com.example.zpils.victoria.Helper.NetworkMonitor;
import com.example.zpils.victoria.Helper.SessionManager;
import com.example.zpils.victoria.Model.Question;
import com.example.zpils.victoria.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by zpils on 2017-08-10.
 */

/**
 * Still need to fix textInputEditTextAnswers click event, that it wouldn't require 2 clicks to
 * activate the onClick event.
 * Also that it wouldn't create new text input if there are empty inputs.
 * Don't allow multiple types of answers, make some validation
 * Fix correct_answers null problem.
 * Extra spaces in open question answers
 *
 * fix activity return after multiple quest_inserts(i will redirect after quest insert to edit_quest or list_quest activity and it should be solved)
 *
 */

public class NewQuestionActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = NewQuestionActivity.this;
    private NestedScrollView nestedScrollView;

    private AppCompatTextView appCompatTextViewCategoryTitle;

    private RadioGroup radioGroupQuestionType;
    private RadioButton radioButtonQuestionType;
    private RadioButton lastButtonQuestionType;
    private TextInputLayout textInputLayoutQuestion;

    private TextInputEditText textInputEditTextQuestion;
    private ArrayList<TextInputEditText> textInputEditTextAnswers = new ArrayList<TextInputEditText>();
    private RadioGroup radioGroupAnswers;
    private ArrayList<CheckBox> checkBoxesAnswers = new ArrayList<CheckBox>();
    private ArrayList<RadioButton> radioButtonsAnswers = new ArrayList<RadioButton>();

    private AppCompatButton appCompatButtonAddQuestion;

    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;
    private AppServerHelper appServerHelper;
    private Question question;
    private SessionManager sessionManager;
    private int category_id;
    private boolean finalQuestionType;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_question);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    public void initViews(){
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        appCompatTextViewCategoryTitle = (AppCompatTextView) findViewById(R.id.appCompatTextViewCategoryTitle);
        Intent intent = getIntent();
        String category_title = intent.getStringExtra("category_title");
        appCompatTextViewCategoryTitle.setText(category_title);

        category_id = intent.getIntExtra("category_id", -1);

        radioGroupQuestionType = (RadioGroup) findViewById(R.id.radioGroupQuestionType);
        int selectedRadioButtonId = radioGroupQuestionType.getCheckedRadioButtonId();
        radioButtonQuestionType = (RadioButton) findViewById(selectedRadioButtonId);
        lastButtonQuestionType = (RadioButton) findViewById(R.id.radioCheckboxQuestion);

        textInputLayoutQuestion = (TextInputLayout) findViewById(R.id.textInputLayoutQuestion);

        textInputEditTextQuestion = (TextInputEditText) findViewById(R.id.textInputEditTextQuestion);
        textInputEditTextAnswers.add((TextInputEditText) findViewById(R.id.textInputEditTextAnswers));
        radioGroupAnswers = new RadioGroup(activity);

        appCompatButtonAddQuestion = (AppCompatButton) findViewById(R.id.appCompatButtonAddQuestion);
    }

    public void initListeners() {
        appCompatButtonAddQuestion.setOnClickListener(this);
        textInputEditTextAnswers.get(0).setOnClickListener(this);
    }

    public void initObjects() {
        inputValidation = new InputValidation(activity);
        databaseHelper = new DatabaseHelper(activity);
        appServerHelper = new AppServerHelper(activity);
        question = new Question();
        sessionManager = new SessionManager(activity);
        finalQuestionType = false;
    }

    @Override
    public void onClick(View v){
        int lastElement = textInputEditTextAnswers.size()-1;
        int lastAnswerID = textInputEditTextAnswers.get(lastElement).getId();

        if (v.getId() == R.id.appCompatButtonAddQuestion){
            addQuestion();
        } else if (v.getId() == lastAnswerID) {
            if (radioButtonQuestionType.getId() == R.id.radioClosedQuestion || radioButtonQuestionType.getId() == R.id.radioCheckboxQuestion){
                if (!textInputEditTextAnswers.get(0).getText().toString().isEmpty()) {
                    addNextAnswer(lastAnswerID);
                }
            }
        }
    }

    private void addQuestion(){
        if (!setQuestion()){
            return;
        }
        if (NetworkMonitor.checkNetworkConnection(activity)){
            question.setSync_status(Question.SYNC_STATUS_OK);
            appServerHelper.addQuestion(question, new AppServerHelper.VolleyCallBack() {
                @Override
                public void onSuccess(String result) {
                    if (result.contains("success")){
                        //adds question to local database
                        int quest_id = Integer.parseInt(result.split(":")[1]);
                        databaseHelper.addQuestion(question,quest_id);

                        //Snack Bar to show success message that record saved successfully
                        Snackbar.make(nestedScrollView, getString(R.string.question_create_success), Snackbar.LENGTH_LONG).show();
                        Intent usersIntent = new Intent(activity, UsersActivity.class);
                        emptyInputEditText();
                        startActivity(usersIntent);
                        finish();
                    } else {
                        //Snack Bar to show error message that something went wrong
                        Snackbar.make(nestedScrollView, getString(R.string.question_create_fail), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            question.setSync_status(Question.SYNC_STATUS_FAILED);
            long rowID = databaseHelper.addQuestion(question,Question.DEFAULT_ID);

            if (rowID == -1) {
                //Snack Bar to show error message that something went wrong
                Snackbar.make(nestedScrollView, getString(R.string.question_create_fail), Snackbar.LENGTH_LONG).show();
            } else {
                //Snack Bar to show success message that record saved successfully
                Snackbar.make(nestedScrollView, getString(R.string.question_create_success), Snackbar.LENGTH_LONG).show();
                Intent usersIntent = new Intent(activity, UsersActivity.class);
                emptyInputEditText();
                startActivity(usersIntent);
                finish();
            }
        }
    }

    private boolean setQuestion(){
        HashMap<String,String> userDetails = sessionManager.getUserDetails();

        if (!checkValues(userDetails)){
            return false;
        }

        question.setFk_category_id(category_id);
        question.setCreator_username(userDetails.get(SessionManager.KEY_USERNAME));
        question.setOwner_username(userDetails.get(SessionManager.KEY_USERNAME));
        question.setQuestion(textInputEditTextQuestion.getText().toString().trim());
        question.setSync_status(Question.SYNC_STATUS_FAILED);

        if (radioButtonQuestionType.getId() == R.id.radioClosedQuestion){
            int all_i = 0;
            int c_i = 0;
            String[] all_answers = new String[radioButtonsAnswers.size()];
            String[] correct_answers = new String[1];
            for (RadioButton rd : radioButtonsAnswers){
                if (rd.isChecked()){
                    correct_answers[c_i] = rd.getText().toString();
                    c_i++;
                }
                all_answers[all_i] = rd.getText().toString();
                all_i++;
            }
            if (all_answers.length < 1)
            {
                textInputEditTextAnswers.get(0).setError(getString(R.string.error_message_answers));
                return false;
            }
            if (c_i < 1){
                radioButtonsAnswers.get(0).setError(getString(R.string.error_message_radio_correct_answer));
                return false;
            }
            question.setType(Question.CLOSED_QUESTION);
            question.setAnswers(all_answers);
            question.setCorrect_answers(correct_answers);

        } else if (radioButtonQuestionType.getId() == R.id.radioCheckboxQuestion){
            int all_i = 0;
            String[] all_answers = new String[checkBoxesAnswers.size()];
            ArrayList<String> correct_answers = new ArrayList<String>();
            for (CheckBox cb : checkBoxesAnswers){
                if (cb.isChecked()){
                    correct_answers.add(cb.getText().toString());
                }
                all_answers[all_i] = cb.getText().toString();
                all_i++;
            }

            //checking if there are answers and correct answers
            if (all_answers.length < 1)
            {
                textInputEditTextAnswers.get(0).setError(getString(R.string.error_message_answers));
                return false;
            }
            if (correct_answers.isEmpty()){
                checkBoxesAnswers.get(0).setError(getString(R.string.error_message_checkbox_correct_answer));
                return false;
            }

            question.setType(Question.CHECKBOX_QUESTION);
            question.setAnswers(all_answers);
            question.setCorrect_answers(correct_answers.toArray(new String[correct_answers.size()]));

        } else if (radioButtonQuestionType.getId() == R.id.radioOpenQuestion){
            String[] all_answers = textInputEditTextAnswers.get(0).getText().toString().trim().split(";");
            String[] correct_answers = textInputEditTextAnswers.get(0).getText().toString().trim().split(";");

            //checking if there are answers and correct answers
            if (!(all_answers.length > 0 && all_answers != null && !all_answers[0].isEmpty()))
            {
                textInputEditTextAnswers.get(0).setError(getString(R.string.error_message_answers));
                return false;
            }
            if (!(correct_answers.length > 0 && correct_answers != null)){
                return false;
            }

            question.setType(Question.OPEN_QUESTION);
            question.setAnswers(all_answers);
            question.setCorrect_answers(correct_answers);
        }
        return true;
    }

    //checks values before adding a question
    private boolean checkValues(HashMap<String,String> userDetails){

        //Question Type Not Selected
        if (radioButtonQuestionType == null){
            lastButtonQuestionType.setError(getString(R.string.error_message_question_type));
            return false;
        }

        //Question is empty
        if (!inputValidation.isInputEditTextFilled(textInputEditTextQuestion,textInputLayoutQuestion,getString(R.string.error_message_question))) {
            return false;
        }

        //There are no answers
        if (radioButtonsAnswers.isEmpty() && checkBoxesAnswers.isEmpty()){
            if (textInputEditTextAnswers.get(0).getText().toString().trim().isEmpty()) {
                textInputEditTextAnswers.get(0).setError(getString(R.string.error_message_answers));
                return false;
            }
            return true;
        }

        //User details not found
        if (userDetails.isEmpty()){
            Snackbar.make(nestedScrollView,getString(R.string.error_user_details_not_found), Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void addNextAnswer(int lastAnswerID){

        //When first answer is added, disables ability to choose question type
        if (!finalQuestionType){
            finalQuestionType = true;
            for (View rd : radioGroupQuestionType.getTouchables()){
                rd.setEnabled(false);
            }
        }

        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompat);
        TextInputEditText textInputEditTextNextAnswer = new TextInputEditText(activity);

        textInputEditTextNextAnswer.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        textInputEditTextNextAnswer.setHint(R.string.hint_enter_the_next_answer);
        textInputEditTextNextAnswer.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));
        textInputEditTextNextAnswer.setHintTextColor(ContextCompat.getColor(activity, R.color.colorText));
        textInputEditTextNextAnswer.setId(findUnusedId(lastAnswerID));
        textInputEditTextNextAnswer.setOnClickListener(this);

        textInputEditTextAnswers.add(textInputEditTextNextAnswer);
        linearLayoutCompat.addView(textInputEditTextNextAnswer,linearLayoutCompat.getChildCount()-1);

        if (radioButtonQuestionType.getId() == R.id.radioClosedQuestion){
            convertToRadiobox(lastAnswerID);
        } else if (radioButtonQuestionType.getId() == R.id.radioCheckboxQuestion){
            convertToCheckbox(lastAnswerID);
        }

    }

    private int findUnusedId(int startID) {
        while( findViewById(++startID) != null );
        return startID;
    }

    private void convertToRadiobox(int lastAnswerID) {
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompat);
        RadioButton radioButton = new RadioButton(activity);
        radioButton.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        radioButton.setId(findUnusedId(lastAnswerID));
        radioButton.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));

        TextInputEditText textInputEditTextConvert = textInputEditTextAnswers.get(0);
        radioButton.setText(textInputEditTextConvert.getText());
        radioGroupAnswers.addView(radioButton);
        radioButtonsAnswers.add(radioButton);

        linearLayoutCompat.removeView(textInputEditTextConvert);
        linearLayoutCompat.removeView(radioGroupAnswers);
        textInputEditTextAnswers.remove(0);
        linearLayoutCompat.addView(radioGroupAnswers,linearLayoutCompat.getChildCount()-2);
    }

    private void convertToCheckbox(int lastAnswerID){
        LinearLayoutCompat linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linearLayoutCompat);
        CheckBox checkBox = new CheckBox(activity);

        checkBox.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        checkBox.setId(findUnusedId(lastAnswerID));
        checkBox.setTextColor(ContextCompat.getColor(activity,R.color.colorTextGray));
        TextInputEditText textInputEditTextConvert = textInputEditTextAnswers.get(0);
        checkBox.setText(textInputEditTextConvert.getText());

        checkBoxesAnswers.add(checkBox);
        linearLayoutCompat.removeView(textInputEditTextConvert);
        textInputEditTextAnswers.remove(0);
        linearLayoutCompat.addView(checkBox,linearLayoutCompat.getChildCount()-2);
    }



    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioOpenQuestion:
                if (checked){
                    radioButtonQuestionType = (RadioButton) findViewById(R.id.radioOpenQuestion);
                }
                break;
            case R.id.radioClosedQuestion:
                if (checked)
                {
                    radioButtonQuestionType = (RadioButton) findViewById(R.id.radioClosedQuestion);
                }
                break;
            case R.id.radioCheckboxQuestion:
                if (checked) {
                    radioButtonQuestionType = (RadioButton) findViewById(R.id.radioCheckboxQuestion);
                }
                break;
        }
    }

    private void emptyInputEditText(){
        radioGroupQuestionType.clearCheck();
        textInputEditTextQuestion.setText(null);
        textInputEditTextAnswers.clear();
        checkBoxesAnswers.clear();
        radioButtonsAnswers.clear();
    }


}
