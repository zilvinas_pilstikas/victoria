package com.example.zpils.victoria.Model;

/**
 * Created by zpils on 2017-08-10.
 */

public class Question {

    public static final int SYNC_STATUS_OK = 1;
    public static final int SYNC_STATUS_FAILED = 0;

    public static final int OPEN_QUESTION = 0;
    public static final int CLOSED_QUESTION = 1;
    public static final int CHECKBOX_QUESTION = 2;

    public static final int DEFAULT_ID = -1;
    public static final String TABLE_QUESTIONS = "questions";
    public static final String COLUMN_ID = "question_id";
    public static final String COLUMN_TYPE = "question_type";
    public static final String COLUMN_QUESTION = "question_question";
    public static final String COLUMN_ANSWERS = "question_answers";
    public static final String COLUMN_CORRECT_ANSWERS = "question_correct_answers";
    public static final String COLUMN_CREATOR_USERNAME = "question_creator_username";
    public static final String COLUMN_OWNER_USERNAME = "question_owner_username";
    public static final String COLUMN_SYNC_STATUS = "question_sync_status";
    public static final String COLUMN_FK_CATEGORY_ID = "fk_category_id";

    public static final String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_FK_CATEGORY_ID + " INTEGER,"
            + COLUMN_TYPE + " INTEGER,"
            + COLUMN_QUESTION + " TEXT,"
            + COLUMN_ANSWERS + " TEXT,"
            + COLUMN_CORRECT_ANSWERS + " TEXT,"
            + COLUMN_CREATOR_USERNAME + " TEXT,"
            + COLUMN_OWNER_USERNAME + " TEXT,"
            + COLUMN_SYNC_STATUS + " INTEGER)";

    public static final String DROP_QUESTIONS_TABLE = "DROP TABLE IF EXISTS" + TABLE_QUESTIONS;

    private int id;
    private int fk_category_id;
    private int type;                  //full answer, y/n (radio), multi-correct(checkbox)
    private String question;
    private String[] correct_answers;
    private String[] answers;
    private String creator_username;            //who created question
    private String owner_username;         //who currently own question
    private int sync_status;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFk_category_id() { return fk_category_id; }

    public void setFk_category_id(int fk_id) { this.fk_category_id = fk_id; }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCreator_username() {
        return creator_username;
    }

    public void setCreator_username(String creator_username) {
        this.creator_username = creator_username;
    }

    public String getOwner_username() {
        return owner_username;
    }

    public void setOwner_username(String owner_username) {
        this.owner_username = owner_username;
    }

    public int getSync_status() {
        return sync_status;
    }

    public void setSync_status(int sync_status) {
        this.sync_status = sync_status;
    }

    public String[] getCorrect_answers() {
        return correct_answers;
    }

    public void setCorrect_answers(String[] correct_answers) {
        this.correct_answers = correct_answers;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }
}
